package com.corvolution.mesana_analyzer_test;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;

import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;

import com.corvolution.mesana_analyzer.Analyzer;
import com.corvolution.mesana_analyzer.analysis_functions.*;
import com.corvolution.mesana_analyzer.data.HostServer;
import com.corvolution.mesana_analyzer.data.Measurement;

/**
 * This class tests the execution order of the Analyzes classes as implemented in the singleAnalysis method.
 * Please note that the order is not always deterministic and depends heavily on the order in which the classes are provided by the configuration file.
 * @author Deyan Mladenov
 *
 */
public class SingleAnalysisTest
{


	/**
	 * Test the singleAnalysis execution order as expected: DTR->QA->Lifestyle->Recommendation->AF
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testSingleAnalysis1()
	{
		/**
		 *  Create Spy object of the analyzes classes. A Spy object only wraps around the real one, and allows for stubbing some of it method calls,
		 *  while all other method calls are executed by the spied class, if called. 
		 */
		
		ArrayList<Analysis> mockedAnalyses = new ArrayList<>();
		DataConverter dConvertSpy = Mockito.spy(new DataConverter());
		mockedAnalyses.add(dConvertSpy);
		MedicalAnalysis medAnalysisSpy = Mockito.spy(new MedicalAnalysis());
		mockedAnalyses.add(medAnalysisSpy);
		QuestionnaireAnalysis questionnaireSpy = spy(new QuestionnaireAnalysis());
		mockedAnalyses.add(questionnaireSpy);
		LifestyleAnalysis lifestyleSpy = Mockito.spy(new LifestyleAnalysis());
		mockedAnalyses.add(lifestyleSpy);
		RecommendationGenerator recommendationSpy = spy(new RecommendationGenerator());
		mockedAnalyses.add(recommendationSpy);
		AfDetection afSpy = spy(new AfDetection());
		mockedAnalyses.add(afSpy);
		
		
		
		String measMock = "";
		ArrayBlockingQueue<HostServer> serverMock = Mockito.mock(ArrayBlockingQueue.class);
		/**
		 * Do nothing when start method is called in all analyzes classes.
		 * All other methods in those classes run as usual.
		 */
		doReturn(true).when(dConvertSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		doReturn(true).when(medAnalysisSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		doReturn(true).when(questionnaireSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		doReturn(true).when(lifestyleSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		doReturn(true).when(recommendationSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		doReturn(true).when(afSpy).start(any(String.class), any(HostServer.class), anyBoolean());

		
		Analyzer analyzerSpy = Mockito.spy(new Analyzer(measMock, serverMock));
		doNothing().when(analyzerSpy).restApiUpdate(any(String.class), any(String.class));
		
		analyzerSpy.singleAnalysis("nothing", mockedAnalyses);
		
		InOrder inOrder = inOrder(dConvertSpy, medAnalysisSpy, questionnaireSpy, lifestyleSpy, recommendationSpy, afSpy);
		
		inOrder.verify(medAnalysisSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		inOrder.verify(questionnaireSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		inOrder.verify(lifestyleSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		inOrder.verify(recommendationSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		inOrder.verify(afSpy).start(any(String.class), any(HostServer.class), anyBoolean());
	}
	
	/**
	 * Test singleAnalysis executing Analyzes in another order. The order given by stubbing the getPrecondition() method is as follows:
	 * (DTR,QA)->Lifestyle->(Recommendation,AF)
	 * Analyzes in brackets do not depend on each other and can be executed in parallel. 
	 * Expected sequential execution order:
	 * DTR->QA->Lifestyle->Recommendation->AF
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testSingleAnalysis2()
	{
		/**
		 *  Create Spy object of the analyzes classes. A Spy object only wraps around the real one, and allows for stubbing some of it method calls,
		 *  while all other method calls are executed by the spied class, if called. 
		 */
		ArrayList<Analysis> mockedAnalyses = new ArrayList<>();
		MedicalAnalysis medAnalysisSpy = Mockito.spy(new MedicalAnalysis());
		mockedAnalyses.add(medAnalysisSpy);
		QuestionnaireAnalysis questionnaireSpy = spy(new QuestionnaireAnalysis());
		mockedAnalyses.add(questionnaireSpy);
		LifestyleAnalysis lifestyleSpy = Mockito.spy(new LifestyleAnalysis());
		mockedAnalyses.add(lifestyleSpy);
		RecommendationGenerator recommendationSpy = spy(new RecommendationGenerator());
		mockedAnalyses.add(recommendationSpy);
		AfDetection afSpy = spy(new AfDetection());
		mockedAnalyses.add(afSpy);
		
	
		/**
		 * Do nothing when start method is called in all analyzes classes.
		 * 
		 */
		doReturn(true).when(medAnalysisSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		doReturn(true).when(questionnaireSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		doReturn(true).when(lifestyleSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		doReturn(true).when(recommendationSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		doReturn(true).when(afSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		
		doReturn(null).when(questionnaireSpy).getPrecondition();
		doReturn(null).when(medAnalysisSpy).getPrecondition();
		ArrayList<Analysis> lifestylePrecond = new ArrayList<>();
		lifestylePrecond.add(medAnalysisSpy);
		lifestylePrecond.add(questionnaireSpy);
		doReturn(lifestylePrecond).when(lifestyleSpy).getPrecondition();
		ArrayList<Analysis> recommendationPrecond = new ArrayList<>();
		recommendationPrecond.add(lifestyleSpy);
		doReturn(recommendationPrecond).when(recommendationSpy).getPrecondition();
		ArrayList<Analysis> afPrecond = new ArrayList<>();
		afPrecond.add(lifestyleSpy);
		doReturn(afPrecond).when(afSpy).getPrecondition();

		String measMock = "";
		ArrayBlockingQueue<HostServer> serverMock = Mockito.mock(ArrayBlockingQueue.class);
		
		Analyzer analyzerSpy = Mockito.spy(new Analyzer(measMock, serverMock));
		doNothing().when(analyzerSpy).restApiUpdate(any(String.class), any(String.class));
	
		analyzerSpy.singleAnalysis("nothing", mockedAnalyses);

		
		InOrder inOrder = inOrder(medAnalysisSpy,questionnaireSpy, lifestyleSpy, recommendationSpy, afSpy);
		
		inOrder.verify(medAnalysisSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		inOrder.verify(questionnaireSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		inOrder.verify(lifestyleSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		inOrder.verify(recommendationSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		inOrder.verify(afSpy).start(any(String.class), any(HostServer.class), anyBoolean());
	}
	
	/**
	 * Test singleAnalysis executing Analyzes in another order. The order given by stubbing the getPrecondition() method is as follows:
	 * (DTR,QA,Lifestyle,Recommendation,AF)
	 * Analyzes in brackets do not depend on each other and can be executed in parallel. 
	 * Expected sequential execution order:
	 * DTR->QA->Lifestyle->Recommendation->AF
	 * In this case the order can be random, but the algorithm follows the order as given in the configuration file.
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testSingleAnalysis3()
	{
		/**
		 *  Create Spy object of the analyzes classes. A Spy object only wraps around the real one, and allows for stubbing some of it method calls,
		 *  while all other method calls are executed by the spied class, if called. 
		 */
		ArrayList<Analysis> mockedAnalyses = new ArrayList<>();
		MedicalAnalysis medAnalysisSpy = Mockito.spy(new MedicalAnalysis());
		mockedAnalyses.add(medAnalysisSpy);
		QuestionnaireAnalysis questionnaireSpy = spy(new QuestionnaireAnalysis());
		mockedAnalyses.add(questionnaireSpy);
		LifestyleAnalysis lifestyleSpy = Mockito.spy(new LifestyleAnalysis());
		mockedAnalyses.add(lifestyleSpy);
		RecommendationGenerator recommendationSpy = spy(new RecommendationGenerator());
		mockedAnalyses.add(recommendationSpy);
		AfDetection afSpy = spy(new AfDetection());
		mockedAnalyses.add(afSpy);
		
		
		/**
		 * Do nothing when start method is called in all analyzes classes.
		 * 
		 */
		doReturn(true).when(medAnalysisSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		doReturn(true).when(questionnaireSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		doReturn(true).when(lifestyleSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		doReturn(true).when(recommendationSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		doReturn(true).when(afSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		
		doReturn(null).when(medAnalysisSpy).getPrecondition();
		
		doReturn(null).when(questionnaireSpy).getPrecondition();
		
		doReturn(null).when(lifestyleSpy).getPrecondition();
		
		doReturn(null).when(recommendationSpy).getPrecondition();
		
		doReturn(null).when(afSpy).getPrecondition();

		String measMock = "";
		ArrayBlockingQueue<HostServer> serverMock = Mockito.mock(ArrayBlockingQueue.class);
		
		Analyzer analyzerSpy = Mockito.spy(new Analyzer(measMock, serverMock));
		doNothing().when(analyzerSpy).restApiUpdate(any(String.class), any(String.class));

		analyzerSpy.singleAnalysis("nothing", mockedAnalyses);
		
		InOrder inOrder = inOrder(medAnalysisSpy,questionnaireSpy, lifestyleSpy, recommendationSpy, afSpy);
		
		inOrder.verify(medAnalysisSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		inOrder.verify(questionnaireSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		inOrder.verify(lifestyleSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		inOrder.verify(recommendationSpy).start(any(String.class), any(HostServer.class), anyBoolean());
		inOrder.verify(afSpy).start(any(String.class), any(HostServer.class), anyBoolean());
	}

}
