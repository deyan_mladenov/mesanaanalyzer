package com.corvolution.mesana_analyzer_test;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.corvolution.mesana_analyzer.Analyzer;
import com.corvolution.mesana_analyzer.Configuration;
import com.corvolution.mesana_analyzer.data.HostServer;
import com.corvolution.mesana_analyzer.utility.ServerInitializer;

public class IntegrationTest1
{
	HostServer server1 = new HostServer(null, 1101);
	HostServer server2 = new HostServer(null, 1102);
	HostServer server3 = new HostServer(null, 1103);
	HostServer server4 = new HostServer(null, 1104);
	HostServer server5 = new HostServer(null, 1105);
	HostServer server6 = new HostServer(null, 1106);
	//HostServer server11 = new HostServer(null, 1101);
	//HostServer server12 = new HostServer(null, 1102);
	/**
	String[] sixmeas = new String[]{"1E9","1F7","1FD", "2E9","2F7","2FD"};
	String[] fivemeas = new String[]{"1E9","1F7","1FD", "2E9","2F7"};
	String[] fourmeas = new String[]{"1E9","1F7","1FD", "2E9"};
	*/
	String[] threemeas = new String[]{"1E9","1F7","1FD"};
	String[] twomeas = new String[]{"1E9","1F7"};
	
	String[] eightmeas = new String[]{"1E9","2E9","3E9","4E9","5E9","6E9","7E9","8E9"};
	String[] sixmeas = new String[]{"1E9","2E9","3E9","4E9","5E9","6E9"};
	String[] fivemeas = new String[]{"1E9","2E9","3E9","4E9","5E9"};
	String[] fourmeas = new String[]{"1E9","2E9","3E9","4E9"};




	
	private Logger log = Logger.getLogger(this.getClass().getName());
	
	public IntegrationTest1()
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy 'at' HH-mm");
		String logfileName = Configuration.getInstance().getLogFilesPath() + sdf.format(new Date()) + " autoTest_log.txt";
		try
		{
			FileHandler fh = new FileHandler(logfileName);
			fh.setLevel(Level.INFO);
			log.addHandler(fh);
			log.setLevel(Level.INFO);
			log.setUseParentHandlers(false);
		}
		catch (SecurityException | IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void runAutoTest()
	{
		//this.executeServerBatch("start2servers.bat");
		List<HostServer> twolist = new ArrayList<HostServer>();
		twolist.add(server1);
		twolist.add(server2);
		ArrayBlockingQueue<HostServer> twoservers = null;
		
		/**
		for (int i = 0; i < 5; i++)
		{
			twoservers = new ArrayBlockingQueue<HostServer>(2, false, twolist);
			this.test2Threads2(twoservers);
		}
		
		//this.executeServerBatch("start3servers.bat");
		List<HostServer> threelist = new ArrayList<HostServer>();
		threelist.add(server1);
		threelist.add(server2);
		threelist.add(server3);
		ArrayBlockingQueue<HostServer> threeservers = null;
		
		for (int i = 0; i < 5; i++)
		{
			threeservers = new ArrayBlockingQueue<HostServer>(3, false, threelist);
			this.test3Threads3(threeservers);
		}
		*/
		//this.executeServerBatch("start4servers.bat");
		
		
		/**
		//this.executeServerBatch("start5servers.bat");
		List<HostServer> fivelist = new ArrayList<HostServer>();
		fivelist.add(server1);
		fivelist.add(server2);
		fivelist.add(server3);
		fivelist.add(server4);
		fivelist.add(server5);

		ArrayBlockingQueue<HostServer> fiveservers = null;
		
		for (int i = 0; i < 5; i++)
		{
			fiveservers = new ArrayBlockingQueue<HostServer>(5, false, fivelist);
			this.test5Threads5(fiveservers);
		}

		
		//this.executeServerBatch("start6servers.bat");
		List<HostServer> sixlist = new ArrayList<HostServer>();
		sixlist.add(server1);
		sixlist.add(server2);
		sixlist.add(server3);
		sixlist.add(server4);
		sixlist.add(server5);
		sixlist.add(server6);

		ArrayBlockingQueue<HostServer> sixservers = null;
		
		for (int i = 0; i < 5; i++)
		{
			sixservers = new ArrayBlockingQueue<HostServer>(6, false, sixlist);
			this.test6Threads6(sixservers);
		}
		*/

		List<HostServer> fourlist = new ArrayList<HostServer>();
		fourlist.add(server1);
		fourlist.add(server2);
		fourlist.add(server3);
		fourlist.add(server4);

		ArrayBlockingQueue<HostServer> fourservers = null;
		
		for (int i = 0; i < 5; i++)
		{
			fourservers = new ArrayBlockingQueue<HostServer>(4, false, fourlist);
			this.test4Threads4(fourservers);
		}
		
		for (int i = 0; i < 4; i++)
		{
			fourservers = new ArrayBlockingQueue<HostServer>(4, false, fourlist);
			this.test4Threads8(fourservers);
		}
		
		List<HostServer> onelist = new ArrayList<HostServer>();
		onelist.add(server1);
		ArrayBlockingQueue<HostServer> oneserver = null;
		
		for (int i = 0; i < 2; i++)
		{
			oneserver = new ArrayBlockingQueue<HostServer>(1, false, onelist);
			this.test1Thread3(oneserver);
		}
		
		return;
	}
	
	
	private void test2Threads2(ArrayBlockingQueue<HostServer> servers) {
		ServerInitializer.initializeServers();
		
		log.log(Level.INFO, "Starting analysis on 2 threads with 2 measurements");
		this.analyseDataSet(twomeas, servers, 2);
		log.log(Level.INFO, "Finished analysis on 2 threads with 2 measurements");
		
		this.quitServers();

	}
	
	private void test3Threads3(ArrayBlockingQueue<HostServer> servers) {
		ServerInitializer.initializeServers();

		log.log(Level.INFO, "Starting analysis on 3 threads with 3 measurements");
		this.analyseDataSet(threemeas, servers, 3);
		log.log(Level.INFO, "Finished analysis on 3 threads with 3 measurements");
		
		this.quitServers();

		
		
	}
	
	private void test4Threads6(ArrayBlockingQueue<HostServer> servers) {
		ServerInitializer.initializeServers();

		log.log(Level.INFO, "Starting analysis on 4 threads with 6 measurements");
		this.analyseDataSet(sixmeas, servers, 4);
		log.log(Level.INFO, "Finished analysis on 4 threads with 6 measurements");
		
		this.quitServers();

	
	}
	
	private void test4Threads4(ArrayBlockingQueue<HostServer> servers) {
		ServerInitializer.initializeServers();

		log.log(Level.INFO, "Starting analysis on 4 threads with 4 measurements");
		this.analyseDataSet(fourmeas, servers, 4);
		log.log(Level.INFO, "Finished analysis on 4 threads with 4 measurements");
		
		this.quitServers();

	}
	
	private void test4Threads8(ArrayBlockingQueue<HostServer> servers) {
		ServerInitializer.initializeServers();

		log.log(Level.INFO, "Starting analysis on 4 threads with 4 measurements");
		this.analyseDataSet(eightmeas, servers, 4);
		log.log(Level.INFO, "Finished analysis on 4 threads with 4 measurements");
		
		this.quitServers();

	
	}
	
	private void test5Threads5(ArrayBlockingQueue<HostServer> servers) {
		ServerInitializer.initializeServers();

		log.log(Level.INFO, "Starting analysis on 5 threads with 5 measurements");
		this.analyseDataSet(fivemeas, servers, 5);
		log.log(Level.INFO, "Finished analysis on 5 threads with 5 measurements");
		
		this.quitServers();

		
	
	}
	
	private void test6Threads6(ArrayBlockingQueue<HostServer> servers) {
		ServerInitializer.initializeServers();

		log.log(Level.INFO, "Starting analysis on 6 threads with 6 measurements");
		this.analyseDataSet(sixmeas, servers, 6);
		log.log(Level.INFO, "Finished analysis on 6 threads with 6 measurements");
		
		this.quitServers();

		
	
	}
	
	private void test1Thread3(ArrayBlockingQueue<HostServer> servers) {
		ServerInitializer.initializeServers();

		log.log(Level.INFO, "Starting analysis on 1 threads with 3 measurements");
		this.analyseDataSet(threemeas, servers, 1);
		log.log(Level.INFO, "Finished analysis on 1 threads with 3 measurements");
		
		this.quitServers();
		
	}
	
	/**
	 * Start analysis with given list of measurements.
	 * @param IdList measurements ID list
	 */
	private void analyseDataSet(String[] IdList, ArrayBlockingQueue<HostServer> servers, int threads)
	{
		
		ExecutorService exec = Executors.newFixedThreadPool(threads);
		
		for (String Id : IdList)
		{
			Analyzer analyzer = new Analyzer(Id, servers);
			
			exec.execute(analyzer);
		}
		
		exec.shutdown();
		
		try
		{
			exec.awaitTermination(40, TimeUnit.MINUTES);
		}
		catch (InterruptedException e)
		{
			
		}
	}
	
	private void executeServerBatch (String name)
	{
		List<String> cmdAndArgs = Arrays.asList("cmd", "/c", "start", "name");
		File dir = new File(Configuration.getInstance().getServerBatchPath());
		
		ProcessBuilder pb = new ProcessBuilder(cmdAndArgs);
		pb.directory(dir);
		try
		{
			Process p = pb.start();
		}
		catch (IOException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try
		{
			TimeUnit.SECONDS.sleep(10);
		}
		catch (InterruptedException e)
		{
			
		}
	}
	
	private void quitServers()
	{
		server1.quitServer();
		server2.quitServer();
		server3.quitServer();
		server4.quitServer();
		server5.quitServer();
		server6.quitServer();
		
		try
		{
			TimeUnit.SECONDS.sleep(10);
		}
		catch (InterruptedException e)
		{
			
		}
	}
}
