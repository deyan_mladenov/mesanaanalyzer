package com.corvolution.mesana_analyzer_test.analysis_functions;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.unisens.Entry;
import org.unisens.Unisens;
import org.unisens.UnisensFactory;
import org.unisens.UnisensFactoryBuilder;
import org.unisens.UnisensParseException;

import com.corvolution.mesana_analyzer.analysis_functions.AbstractAnalysis;
import com.corvolution.mesana_analyzer.analysis_functions.AfDetection;
import com.corvolution.mesana_analyzer.analysis_functions.LifestyleAnalysis;
import com.corvolution.mesana_analyzer.data.HostServer;

public class DeleteDataTest
{
	
	private final static String SOURCE_PATH1 = "C:\\Users\\mladenov\\Documents\\DataSetsReadOnly\\jUnit\\Mesana_afAnalysis_deleteDataTest";
	
	private final static String DATA_PATH1 = "C:\\Users\\mladenov\\Documents\\DataSets\\jUnit\\Mesana_afAnalysis_deleteDataTest";
	
	private final static String SOURCE_PATH2 = "C:\\Users\\mladenov\\Documents\\DataSetsReadOnly\\jUnit\\Mesana_lifestyle_deleteDataTest";
	
	private final static String DATA_PATH2 = "C:\\Users\\mladenov\\Documents\\DataSets\\jUnit\\Mesana_lifestyle_deleteDataTest";
	
	private final static String SOURCE_PATH3 = "C:\\Users\\mladenov\\Documents\\DataSetsReadOnly\\jUnit\\Mesana_539ECE9F44A1A_entryDeleteTest";
	
	private final static String DATA_PATH3 = "C:\\Users\\mladenov\\Documents\\DataSets\\jUnit\\Mesana_539ECE9F44A1A_entryDeleteTest";
	
	private class AfDetectionSubclass extends AfDetection
	{
		
		public ArrayList<String> customAttributesKeyList = super.customAttributesKeyList;
		public ArrayList<String> entryIdList = super.entryIdList; 
		public AfDetectionSubclass()
		{
			super();
			super.measurementJsonIdList = new ArrayList<String>(Arrays.asList("SUBJECT_GENDER","SUBJECT_AGE","SUBJECT_HEIGHT","SUBJECT_WEIGHT","ANAMNESIS_DRUGS","ANAMNESIS_CARDIAC_DISEASES",
					"MEASUREMENT_START","MEASUREMENT_DURATION","MEASUREMENT_SHORT","STRESS_SYMPTOMS","STRESS_FEELING","RISK_STROKE","RISK_INFARCTION","ECG_HR_MIN","SLEEP_RECOVERING",
					"STRESS_HRV_AGE","STRESS_BALANCE_LOAD","STRESS_BALANCE_RELAX","STRESS_BALANCE","SLEEP_DURATION","ACTIVITY_DAILY","ACTIVITY_DIVERSITY","SUBJECT_BMI","ACTIVITY_INTENSITY",
					"ACTIVITY_STEPS","ACTIVITY_STEPS_DAY1","ACTIVITY_STEPS_DAY2","SLEEP_PSQI","SLEEP_PSQI_QUALITY","SLEEP_PSQI_LATENCY","SLEEP_PSQI_DURATION","SLEEP_PSQI_EFFICIENCY",
					"SLEEP_PSQI_DISTURBANCES","SLEEP_PSQI_MEDICATION","SLEEP_PSQI_DAILYDISFUNCTION","STRESS_HRV_TABLE_DAY1","STRESS_HRV_TABLE_DAY2","FINDRISK_SPORTS","FINDRISK_FOOD",
					"ANAMNESIS_DISEASES","ANAMNESIS_PACER","ANAMNESIS_ARRHYTHMIA","ANAMNESIS_SPORTS","ANAMNESIS_SYSTRR","ANAMNESIS_SMOKING","ANAMNESIS_DIABETES","STRESS_SYMPTOM_LIST",
					"STRESS_SYMPTOM_LIST_N","STRESS_STRESSOR_LIST","STRESS_STRESSOR_LIST_N","RISK_DIABETES","ACTIVITY_CLASS_DURATION_UNKNOWN_DAY1","ACTIVITY_CLASS_DURATION_LYING_DAY1",
					"ACTIVITY_CLASS_DURATION_UPRIGHT_DAY1","ACTIVITY_CLASS_DURATION_ACTIVE_DAY1","ACTIVITY_CLASS_DURATION_SPORTS_DAY1","ACTIVITY_CLASS_DURATION_UNKNOWN_DAY2","ACTIVITY_CLASS_DURATION_LYING_DAY2",
					"ACTIVITY_CLASS_DURATION_UPRIGHT_DAY2","ACTIVITY_CLASS_DURATION_ACTIVE_DAY2","ACTIVITY_CLASS_DURATION_SPORTS_DAY2","SUMMARY_SLEEP","SUMMARY_STRESS","SUMMARY_ACTIVITY",
					"SUMMARY_RISK","ACTIVITY_STEPS_2","ECG_HR_MIN_4","SLEEP_RECOVERING_1","SLEEP_RECOVERING_3","ACTIVITY_DAILY_1","ACTIVITY_DIVERSITY_3","STRESS_FEELING_2","ACTIVITY_INTENSITY_3",
					"SUBJECT_BMI_2","STRESS_HRV_AGE_0","SLEEP_DURATION_3","STRESS_BALANCE_1","SLEEP_PSQI_2","STRESS_STRESSOR_LIST_3","STRESS_SYMPTOM_LIST_1","RISK_DIABETES_1","ECG_HR_MIN_RECOMMENDATION",
					"ACTIVITY_INTENSITY_RECOMMENDATION","ACTIVITY_DAILY_RECOMMENDATION","ACTIVITY_DIVERSITY_RECOMMENDATION","SLEEP_DURATION_RECOMMENDATION","SUBJECT_BMI_RECOMMENDATION",
					"SLEEP_RECOVERING_RECOMMENDATION","STRESS_STRESSOR_LIST_TEXT","STRESS_SYMPTOM_LIST_TEXT","STRESS_HRV_AGE_RECOMMENDATION","STRESS_BALANCE_RECOMMENDATION","ACTIVITY_STEPS_RECOMMENDATION",
					"SLEEP_PSQI_RECOMMENDATION","STRESS_FEELING_RECOMMENDATION","RISK_DIABETES_RECOMMENDATION"));
		}
		
		public void deleteData() throws UnisensParseException, IOException
		{
			 super.deleteData();
		}
	}
	private class LifestyleSubclass extends LifestyleAnalysis
	{
		public ArrayList<String> filesList = super.filesList;
		
		public LifestyleSubclass()
		{
			super();
		}
		
		public void deleteData() throws UnisensParseException, IOException
		{
			super.deleteData();
		}
	}
	
	private class DeleteEntrySubclass extends AbstractAnalysis
	{
		public ArrayList<String> getAttributes()
		{
			return this.customAttributesKeyList;
		}
		
		public ArrayList<String> getEntryList()
		{
			return this.entryIdList;
		}
		
		public DeleteEntrySubclass()
		{
			super.customAttributesKeyList = new ArrayList<String>(Arrays.asList("weight","age","gender"));
			super.filesList = null;
			super.filesToMove = null;
			super.entryIdList = new ArrayList<String>(Arrays.asList("pause2.csv","tachogram2.csv","ecg_trigger2.csv","ecg_artifact2.csv","ecg_artifact2_val.csv","ecg_artifact.csv",
					"ecg_artifact_val.csv","ecg_rpeak_delays.csv","ecg_trigger.csv","ecg_quality_param_ch5.csv","ecg_quality_rating_ch5.csv"));
			
		}
		
		public void deleteData() throws UnisensParseException, IOException
		{
			super.deleteData();
		}


		@Override
		public boolean equals(Object arg0)
		{
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean start(String pathToMeasurement, HostServer host, boolean overwrite)
		{
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		protected boolean executeLocally(String pathToMeasurement)
		{
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		protected boolean executeRemotely(String pathToMeasurement, HostServer server)
		{
			// TODO Auto-generated method stub
			return false;
		}

	}

	@BeforeClass
	public static void copyData() throws IOException 
	{
		File source = new File(SOURCE_PATH1);
		File dest = new File(DATA_PATH1);
		FileUtils.copyDirectory(source, dest);
		File source2 = new File(SOURCE_PATH2);
		File dest2 = new File(DATA_PATH2);
		FileUtils.copyDirectory(source2, dest2);
		File source3 = new File(SOURCE_PATH3);
		File dest3 = new File(DATA_PATH3);
		FileUtils.copyDirectory(source3, dest3);
	}
	
	@Test
	public void testAf() throws UnisensParseException, IOException
	{
		AfDetectionSubclass afAnalysis = new AfDetectionSubclass();
		afAnalysis.setMeasurementIdPath(DATA_PATH1);
		
		
		afAnalysis.deleteData();
		assertTrue(this.checkAttributesDeleted(DATA_PATH1, afAnalysis.customAttributesKeyList));
		assertTrue(this.checkEntriesDeleted(DATA_PATH1, afAnalysis.entryIdList));
		
		// Try to delete data, that should be deleted already.
		afAnalysis.deleteData();
		assertTrue(this.checkAttributesDeleted(DATA_PATH1, afAnalysis.customAttributesKeyList));
		assertTrue(this.checkEntriesDeleted(DATA_PATH1, afAnalysis.entryIdList));
		
	}
	
	@Test
	public void testLifestyle() throws UnisensParseException, IOException
	{
		LifestyleSubclass lifestyle = new LifestyleSubclass();
		lifestyle.setMeasurementIdPath(DATA_PATH2);
		
		lifestyle.deleteData();
		assertTrue(this.checkFilesDeleted(DATA_PATH2, lifestyle.filesList));

	}
	
	@Test
	public void test3UnisensDeleteEntries() throws UnisensParseException, IOException
	{
		DeleteEntrySubclass entrysubclass = new DeleteEntrySubclass();
		entrysubclass.setMeasurementIdPath(DATA_PATH3);
		
		entrysubclass.deleteData();
		assertTrue(DeleteDataTest.checkAttributesDeleted(DATA_PATH3, entrysubclass.getAttributes()));
		assertTrue(DeleteDataTest.checkEntriesDeleted(DATA_PATH3, entrysubclass.getEntryList()));
		
		// Try to delete data, that should be deleted already.
		entrysubclass.deleteData();
		assertTrue(DeleteDataTest.checkAttributesDeleted(DATA_PATH3, entrysubclass.getAttributes()));
		assertTrue(DeleteDataTest.checkEntriesDeleted(DATA_PATH3, entrysubclass.getEntryList()));
	}
	
	private static boolean checkFilesDeleted(String measurementIdPath, ArrayList<String> filesList)
	{
		for (int i = 0; i < filesList.size(); i++)
		{
			Path path = Paths.get(measurementIdPath, filesList.get(i));
			if (Files.exists(path))
			{
				return false;
			}
		}
		return true;
	}
	
	private static boolean checkAttributesDeleted(String measurementIdPath, ArrayList<String> customAttributesKeyList) throws UnisensParseException
	{
		UnisensFactory uf = UnisensFactoryBuilder.createFactory();
		Unisens u = uf.createUnisens(measurementIdPath);
		Set<String> list = u.getCustomAttributes().keySet();
		if (list == null)
		{
			return true;
		}
		Iterator<String> iter = customAttributesKeyList.iterator();
		while (iter.hasNext())
		{
			String current = iter.next();
			if (list.contains(current))
			{
				return false;
			}
		}
		
		return true;
	}
	
	private static boolean checkEntriesDeleted(String measurementIdPath, ArrayList<String> entryIdList) throws UnisensParseException
	{
		UnisensFactory uf = UnisensFactoryBuilder.createFactory();
		Unisens u = uf.createUnisens(measurementIdPath);
		ArrayList<Entry> list = (ArrayList<Entry>) u.getEntries();
		if (list == null)
		{
			return true;
		}
		ArrayList<String> unisensIdList = new ArrayList<String>();
		for (int i = 0; i < list.size(); i++)
		{
			String entry = list.get(i).getId();
			unisensIdList.add(list.get(i).getId());
		}
		for (int i = 0; i < entryIdList.size(); i++) 
		{
			String current = entryIdList.get(i);

			if (unisensIdList.contains(entryIdList.get(i))) 
			{
				
				return false;
			}
		}
		return true;
	}
}
