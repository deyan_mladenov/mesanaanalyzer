package com.corvolution.mesana_analyzer_test.analysis_functions;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.unisens.DuplicateIdException;
import org.unisens.UnisensParseException;

import com.corvolution.mesana_analyzer.analysis_functions.LifestyleAnalysis;

public class MoveDataTest
{
	
	private final static String SOURCE_PATH1 = "C:\\Users\\mladenov\\Documents\\DataSetsReadOnly\\jUnit\\Mesana_D1FD_before_moveDataTest";
	
	private final static String DATA_PATH1 = "C:\\Users\\mladenov\\Documents\\DataSets\\jUnit\\Mesana_D1FD_before_moveDataTest";
	
	private final static String SOURCE_PATH2 = "C:\\Users\\mladenov\\Documents\\DataSetsReadOnly\\jUnit\\Mesana_55EF1C33ED1FD_result_moveDataTest";
	
	private final static String DATA_PATH2 = "C:\\Users\\mladenov\\Documents\\DataSets\\jUnit\\Mesana_55EF1C33ED1FD_result_moveDataTest";

	private class LifestyleSubclass extends LifestyleAnalysis
	{
		public LifestyleSubclass()
		{
			super();
		}
		
		public void moveDataToCurrent(String sourcePath) throws UnisensParseException, IOException
		{
			super.moveDataToCurrent(sourcePath);;
		}
		
		public boolean checkResultDataExists() throws FileNotFoundException, UnisensParseException
		{
			return super.checkResultDataExists();
		}
	}
	
	@BeforeClass
	public static void copyData() throws IOException 
	{
		File source = new File(SOURCE_PATH1);
		File dest = new File(DATA_PATH1);
		FileUtils.copyDirectory(source, dest);
		File source2 = new File(SOURCE_PATH2);
		File dest2 = new File(DATA_PATH2);
		FileUtils.copyDirectory(source2, dest2);
	}
	
	@Test
	public void test1() throws UnisensParseException, IOException, DuplicateIdException 
	{
		LifestyleSubclass lifestyle = new LifestyleSubclass();
		lifestyle.setMeasurementIdPath(DATA_PATH1);
		lifestyle.moveDataToCurrent(DATA_PATH2);
		assertTrue(lifestyle.checkResultDataExists());
	}
}
