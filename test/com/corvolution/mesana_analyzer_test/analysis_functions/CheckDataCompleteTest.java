package com.corvolution.mesana_analyzer_test.analysis_functions;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;
import org.unisens.UnisensParseException;

import com.corvolution.mesana_analyzer.analysis_functions.AbstractAnalysis;
import com.corvolution.mesana_analyzer.analysis_functions.AfDetection;
import com.corvolution.mesana_analyzer.analysis_functions.Analysis;
import com.corvolution.mesana_analyzer.analysis_functions.LifestyleAnalysis;
import com.corvolution.mesana_analyzer.data.HostServer;

public class CheckDataCompleteTest
{
	
	private class AfDetectionSubclass extends AfDetection
	{
		public AfDetectionSubclass()
		{
			super();
			
		}
		
		public boolean checkResultDataExists() throws FileNotFoundException, UnisensParseException
		{
			 return super.checkResultDataExists();
		}
	}
	private class LifestyleSubclass extends LifestyleAnalysis
	{
		public LifestyleSubclass()
		{
			super();
		}
		
		public boolean ckeckData() throws FileNotFoundException, UnisensParseException
		{
			return super.checkResultDataExists();
		}
	}
	
	private class JsonTestSubclass extends AbstractAnalysis
	{
		public JsonTestSubclass()
		{
			super();
			super.measurementJsonIdList = new ArrayList<String>(Arrays.asList("SUBJECT_GENDER","SUBJECT_AGE","SUBJECT_HEIGHT","SUBJECT_WEIGHT","ANAMNESIS_DRUGS","ANAMNESIS_CARDIAC_DISEASES",
					"MEASUREMENT_START","MEASUREMENT_DURATION","MEASUREMENT_SHORT","STRESS_SYMPTOMS","STRESS_FEELING","RISK_STROKE","RISK_INFARCTION","ECG_HR_MIN","SLEEP_RECOVERING",
					"STRESS_HRV_AGE","STRESS_BALANCE_LOAD","STRESS_BALANCE_RELAX","STRESS_BALANCE","SLEEP_DURATION","ACTIVITY_DAILY","ACTIVITY_DIVERSITY","SUBJECT_BMI","ACTIVITY_INTENSITY",
					"ACTIVITY_STEPS","ACTIVITY_STEPS_DAY1","ACTIVITY_STEPS_DAY2","SLEEP_PSQI","SLEEP_PSQI_QUALITY","SLEEP_PSQI_LATENCY","SLEEP_PSQI_DURATION","SLEEP_PSQI_EFFICIENCY",
					"SLEEP_PSQI_DISTURBANCES","SLEEP_PSQI_MEDICATION","SLEEP_PSQI_DAILYDISFUNCTION","STRESS_HRV_TABLE_DAY1","STRESS_HRV_TABLE_DAY2","FINDRISK_SPORTS","FINDRISK_FOOD",
					"ANAMNESIS_DISEASES","ANAMNESIS_PACER","ANAMNESIS_ARRHYTHMIA","ANAMNESIS_SPORTS","ANAMNESIS_SYSTRR","ANAMNESIS_SMOKING","ANAMNESIS_DIABETES","STRESS_SYMPTOM_LIST",
					"STRESS_SYMPTOM_LIST_N","STRESS_STRESSOR_LIST","STRESS_STRESSOR_LIST_N","RISK_DIABETES","ACTIVITY_CLASS_DURATION_UNKNOWN_DAY1","ACTIVITY_CLASS_DURATION_LYING_DAY1",
					"ACTIVITY_CLASS_DURATION_UPRIGHT_DAY1","ACTIVITY_CLASS_DURATION_ACTIVE_DAY1","ACTIVITY_CLASS_DURATION_SPORTS_DAY1","ACTIVITY_CLASS_DURATION_UNKNOWN_DAY2","ACTIVITY_CLASS_DURATION_LYING_DAY2",
					"ACTIVITY_CLASS_DURATION_UPRIGHT_DAY2","ACTIVITY_CLASS_DURATION_ACTIVE_DAY2","ACTIVITY_CLASS_DURATION_SPORTS_DAY2","SUMMARY_SLEEP","SUMMARY_STRESS","SUMMARY_ACTIVITY",
					"SUMMARY_RISK","ACTIVITY_STEPS_2","ECG_HR_MIN_4","SLEEP_RECOVERING_1","SLEEP_RECOVERING_3","ACTIVITY_DAILY_1","ACTIVITY_DIVERSITY_3","STRESS_FEELING_2","ACTIVITY_INTENSITY_3",
					"SUBJECT_BMI_2","STRESS_HRV_AGE_0","SLEEP_DURATION_3","STRESS_BALANCE_1","SLEEP_PSQI_2","STRESS_STRESSOR_LIST_3","STRESS_SYMPTOM_LIST_1","RISK_DIABETES_1","ECG_HR_MIN_RECOMMENDATION",
					"ACTIVITY_INTENSITY_RECOMMENDATION","ACTIVITY_DAILY_RECOMMENDATION","ACTIVITY_DIVERSITY_RECOMMENDATION","SLEEP_DURATION_RECOMMENDATION","SUBJECT_BMI_RECOMMENDATION",
					"SLEEP_RECOVERING_RECOMMENDATION","STRESS_STRESSOR_LIST_TEXT","STRESS_SYMPTOM_LIST_TEXT","STRESS_HRV_AGE_RECOMMENDATION","STRESS_BALANCE_RECOMMENDATION","ACTIVITY_STEPS_RECOMMENDATION",
					"SLEEP_PSQI_RECOMMENDATION","STRESS_FEELING_RECOMMENDATION","RISK_DIABETES_RECOMMENDATION"));
		}
		
		public boolean ckeckData() throws FileNotFoundException, UnisensParseException
		{
			return super.checkResultDataExists();
		}


		@Override
		public boolean equals(Object arg0)
		{
			// TODO Auto-generated method stub
			return false;
		}

	

		
		protected boolean executeRemotely()
		{
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean start(String pathToMeasurement, HostServer host, boolean overwrite)
		{
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		protected boolean executeLocally(String pathToMeasurement)
		{
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		protected boolean executeRemotely(String pathToMeasurement, HostServer server)
		{
			// TODO Auto-generated method stub
			return false;
		}

	
		
	}
	
	@Test
	public void testAf() throws FileNotFoundException, UnisensParseException
	{
		AfDetectionSubclass afAnalysis = new AfDetectionSubclass();
		afAnalysis.setMeasurementIdPath("C:\\Users\\mladenov\\Documents\\DataSetsReadOnly\\jUnit\\Mesana_afAnalysis_deleteDataTest");
		assertTrue(afAnalysis.checkResultDataExists());
	}
	
	@Test
	public void testLifestyle() throws FileNotFoundException, UnisensParseException
	{
		LifestyleSubclass lifestyle = new LifestyleSubclass();
		lifestyle.setMeasurementIdPath("C:\\Users\\mladenov\\Documents\\DataSetsReadOnly\\unisensData\\Mesana_5644A79DF03E9");
		assertTrue(lifestyle.ckeckData());
	}
	
	@Test
	public void testJson() throws FileNotFoundException, UnisensParseException
	{
		JsonTestSubclass jsonTest = new JsonTestSubclass();
		
		jsonTest.setMeasurementIdPath("C:\\Users\\mladenov\\Documents\\DataSetsReadOnly\\jUnit\\Mesana_afAnalysis_deleteDataTest");
		assertTrue(jsonTest.ckeckData());
	}
}
