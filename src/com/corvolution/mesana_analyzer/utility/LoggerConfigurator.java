package com.corvolution.mesana_analyzer.utility;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import com.corvolution.mesana_analyzer.Configuration;


public class LoggerConfigurator
{
	
	private static Logger errorLog = Logger.getLogger(LoggerConfigurator.class.getName());
	
	private static Logger debugLog = Logger.getLogger("debugLog");
	
	public static void setup() 
	{
		Logger rootLogger = Logger.getLogger("");
		System.out.println("rootLogger: " + rootLogger.toString());
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy 'at' HH-mm");
		String statelogfileName = Configuration.getInstance().getLogFilesPath() + sdf.format(new Date()) + " state_log.txt";
		
		String errorlogfileName = Configuration.getInstance().getLogFilesPath() + sdf.format(new Date()) + " error_log.txt";
		
		String debuglogfileName = Configuration.getInstance().getLogFilesPath() + sdf.format(new Date()) + " debug_log.txt";
		
		
		try
		{
			FileHandler statefileHandler = new FileHandler(statelogfileName);
			statefileHandler.setFormatter(new SimpleFormatter());
			rootLogger.addHandler(statefileHandler);
			rootLogger.setLevel(Level.INFO);
			
			FileHandler errorfileHandler = new FileHandler(errorlogfileName);
			//errorfileHandler.setFormatter(new SimpleFormatter());
			errorfileHandler.setLevel(Level.INFO);
			/**
			FileHandler debugfileHandler = new FileHandler(debuglogfileName);
			debugfileHandler.setLevel(Level.INFO);
			debugLog.addHandler(debugfileHandler);
			debugLog.setUseParentHandlers(false);
			debugLog.setLevel(Level.ALL);
			*/
			ConsoleHandler cmdHandler = new ConsoleHandler();
			cmdHandler.setFormatter(new SimpleFormatter());
			cmdHandler.setLevel(Level.ALL);
			errorLog.addHandler(errorfileHandler);
			errorLog.addHandler(cmdHandler);
			errorLog.setUseParentHandlers(false);
			errorLog.setLevel(Level.ALL);
		}
		catch (SecurityException | IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Logger getErrorLogger()
	{
		return errorLog;
	}
	
	public static Logger getDebugLogger()
	{
		return debugLog;
	}

}
