package com.corvolution.mesana_analyzer.utility;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.corvolution.mesana_analyzer.Configuration;

public class ServerInitializer
{

	public static void initializeServers()
	{
		List<String> cmdAndArgs = Arrays.asList("cmd", "/c", "start", "run_local_server.bat");
		File dir = new File(Configuration.getInstance().getServerBatchPath());
		
		ProcessBuilder pb = new ProcessBuilder(cmdAndArgs);
		pb.directory(dir);
		try
		{
			Process p = pb.start();
		}
		catch (IOException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			TimeUnit.SECONDS.sleep(12);
		}
		catch (InterruptedException e)
		{
		}
	}
	
}
