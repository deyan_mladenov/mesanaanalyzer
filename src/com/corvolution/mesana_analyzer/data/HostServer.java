package com.corvolution.mesana_analyzer.data;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import mesana.serverexit.ServerShutdown;

/**
 * This contains fields for defining a server. Server is defined by a name and a port.
 * @author mladenov
 *
 */
public class HostServer
{
	//port number e.g. 1101/1102 etc.
	private int port;
	
	// host IP address (or domain name)
	private String host;
	
	private Registry reg = null;
	
	public HostServer(String host, int port)
	{
		this.port = port;
		
		this.host = host;
	}
	
	public int getPort()
	{
		return this.port;
	}

	public String getHost()
	{
		return host;
	}

	@Override
	public boolean equals(Object arg0)
	{
		if (arg0 == null || !(arg0 instanceof HostServer))
		{
			return false;
		}
		else
		{
			HostServer other = (HostServer) arg0;
			if (other.getPort() == this.port && other.getHost().equals(this.host))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
	}
	
	/**
	 * Returns a reference to this server's registry.
	 * @return
	 */
	public Registry getRegistry()
	{
		if (this.reg == null)
		{
			
			try
			{
				if (host == null) 
				{
				this.reg = LocateRegistry.getRegistry(port);
				}
				else
				{
					this.reg = LocateRegistry.getRegistry(host, port);
				}
			}
			catch (RemoteException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return this.reg;
		}
		else
		{
			return this.reg;
		}
	}
	
	/**
	 * Quit the RMI server application.
	 */
	public void quitServer()
	{
		Registry reg = this.getRegistry();
		try
		{
			ServerShutdown quit = (ServerShutdown) reg.lookup("SERVER");
			quit.exitServer();
		}
		catch (RemoteException | NotBoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public String toString()
	{
		//if host is null, writes local as host name, the same way as defined in the properties file
		String result = (this.host == null? "local" : this.host) + ", " + this.port;
		return result;
	}

}
