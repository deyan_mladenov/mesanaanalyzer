package com.corvolution.mesana_analyzer;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONObject;

import com.corvolution.mesana_analyzer.data.HostServer;
import com.corvolution.mesana_analyzer.data.Measurement;
import com.corvolution.mesana_analyzer.data.MeasurementCollection;
import com.corvolution.mesana_analyzer.utility.LoggerConfigurator;
import com.corvolution.mesana_analyzer.utility.RestApiConnector;

/**
 * This class obtains measurements list from the REST Api and creates Runnable analyzes that are passed to a fixed threadpool executor.
 * The class itsels implements the runnable interface, in order to be used in a scheduled execution.
 * @author Deyan Mladeenov
 *
 */
public class AnalyzerExecutor implements Runnable
{

	/** List of all available servers to execute functions */
	private final ArrayBlockingQueue<HostServer> servers;
	
	/** Global pointer to current server in list to use */
	int pointer;
	
	/** Initialize an executor to run analyzes in parallel. */
	public AnalyzerExecutor()
	{
		List<HostServer> serverList = Configuration.getInstance().getServers();
		this.servers = new ArrayBlockingQueue<HostServer>(serverList.size(), false, serverList);
		this.pointer = 0;
	}
	
	@Override
	public void run()
	{
		startAnalyzerService();
		
	}
	
	/**
	 * Start the parallel execution of analyzes, by getting measurements from REST.
	 */
	public void startAnalyzerService() 
	{
		ExecutorService executorService = Executors.newFixedThreadPool(Configuration.getInstance().getThreadsCount());
		
		List<Measurement> measurements = this.getMeasurementsForAnalysis();
		
		for (int i = 0; i < measurements.size(); i++)
		{
			Analyzer analyzer = new Analyzer(measurements.get(i).getId(), servers);
			
			executorService.execute(analyzer);
		}
		
		executorService.shutdown();
	}
	
	/**
	 * Start analysis with given list of measurements.
	 * @param IdList measurements ID list
	 */
	public void analyseDataSet(String[] IdList)
	{
		ExecutorService exec = Executors.newFixedThreadPool(Configuration.getInstance().getThreadsCount());
		
		for (String Id : IdList)
		{
			Analyzer analyzer = new Analyzer(Id, this.servers);
			
			exec.execute(analyzer);
		}
		
		exec.shutdown();
	}
	/**
	 * Get next server from list.
	 * @return
	 
	private HostServer getNextServer()
	{
		if (this.pointer == this.servers.size())
		{
			this.pointer = 0;
		}
		
		HostServer server = servers.get(pointer);
		this.pointer++;
		return server;
	}*/
	
	
	public List<Measurement> getMeasurementsForAnalysis() 
	{
		MeasurementCollection mCollect = new MeasurementCollection();
		
		String url = Configuration.getInstance().getRestPath() + "measurements?state=WAIT_FOR_ANALYSIS";
		
		mCollect.setList(url);
		
		return mCollect.getList();
		
	}
	
	
	public void restApiUpdate(String state, String mId)
	{
		RestApiConnector restApi = new RestApiConnector();
		JSONObject json = new JSONObject();
		json.put("state", state);
		String login = "";
		json.put("user", login);
		String jsonString = json.toJSONString();
		String sURL = Configuration.getInstance().getRestPath() + "measurements/" + mId + "/";
		try
		{
			restApi.putMethod(jsonString, sURL);
		}
		catch (Exception e)
		{
			Logger errorLog = LoggerConfigurator.getErrorLogger();
			errorLog.log(Level.SEVERE, "Error thrown when trying to get list of measurements waiting for analysis", e);
			//e.printStackTrace();
		}
	}

	
}
