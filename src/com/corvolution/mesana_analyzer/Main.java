package com.corvolution.mesana_analyzer;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import javax.json.*;

import org.json.simple.JSONObject;
import org.unisens.Entry;
import org.unisens.Unisens;
import org.unisens.UnisensFactory;
import org.unisens.UnisensFactoryBuilder;

//import org.unisens.*;

import com.corvolution.mesana_analyzer.analysis_functions.*;
import com.corvolution.mesana_analyzer.data.HostServer;
import com.corvolution.mesana_analyzer.data.Measurement;
import com.corvolution.mesana_analyzer.utility.LoggerConfigurator;
import com.corvolution.mesana_analyzer.utility.RestApiConnector;
import com.corvolution.mesana_analyzer.utility.ServerInitializer;
import com.corvolution.mesana_analyzer_test.IntegrationTest1;
import com.mathworks.toolbox.javabuilder.*;

import afdetection.MatlabAfDetection;
import convert_ekgMove3_to_DekomTex.Class1;
import dtrstarter.DTRStart;
import mesana.dataanalyzer.LifestyleAnalysisRemote;
import mesana.serverexit.ServerShutdown;

import com.mathworks.toolbox.javabuilder.MWArray;
import com.mathworks.toolbox.javabuilder.MWCharArray;
import com.mathworks.toolbox.javabuilder.MWException;
import com.mathworks.toolbox.javabuilder.MWNumericArray;

/**
 * This class starts the program. The program can run in two modes- scheduled execution, when no input arguments were given, 
 * or data set execution, when the user provides list with measurements as input.
 * @author mladenov
 *
 */
public class Main {

	public Main() {
	}

	public static void main(String[] args)
	{
		LoggerConfigurator.setup();
		
		
		ServerInitializer.initializeServers();
		
		if (args.length == 0) //args are the input arguments, that can be provided when executing the application from the cmd
		{
			ExecutionScheduler eSched = new ExecutionScheduler();
			eSched.scheduledExecution();
		}
		else
		{
			AnalyzerExecutor analyzerExec = new AnalyzerExecutor();
			analyzerExec.analyseDataSet(args);
			
		}
		
		/**
		IntegrationTest1 bigTest1 = new IntegrationTest1();
		bigTest1.runAutoTest();
		*/
		
		/**
		HostServer server2 = new HostServer(null, 1102);
		HostServer server1 = new HostServer(null, 1101);
		HostServer server3 = new HostServer(null, 1103);
		HostServer server4 = new HostServer(null, 1104);
		*/
		/**
		Analyzer analyzer11 = new Analyzer("55154F743EAF8" , server1);
		Analyzer analyzer12 = new Analyzer("53C3CD8EA00F7", server2);
		Analyzer analyzer13 = new Analyzer("55DFF16992AE2" , server3);
		Analyzer analyzer14 = new Analyzer("5515536788D2C" , server4);
		*/
		/**
		QuestionnaireAnalysis qaA = new QuestionnaireAnalysis();
		qaA.start("C:\\Users\\mladenov\\Documents\\DataSets\\Integration\\Mesana_55154F743EAF8" , null, true);
		qaA.start("C:\\Users\\mladenov\\Documents\\DataSets\\Integration\\Mesana_5515536788D2C", null, true);
		*/
		
		/**
		analyzer11.run();
		analyzer12.run();
		analyzer13.run();
		analyzer14.run();
		*/
		/**
		ExecutorService exec = Executors.newFixedThreadPool(4);
		exec.execute(analyzer11);
		exec.execute(analyzer12);
		exec.execute(analyzer13);
		exec.execute(analyzer14);
		*/

		/**
		Thread d11 = new Thread(analyzer11);
		Thread d12 = new Thread(analyzer12);
		Thread d13 = new Thread(analyzer13);
		Thread d14 = new Thread(analyzer14);

		d11.start();
		d12.start();
		d13.start();
		d14.start();
		*/
		/**
		ExecutionScheduler eSched = new ExecutionScheduler();
		eSched.scheduledExecution();
		*/
		
		/**
		HostServer server11 = new HostServer("129.13.72.198", 1103);
		
		Registry reg = server11.getRegistry();
		
		try
		{
			LifestyleAnalysisRemote ls = (LifestyleAnalysisRemote) reg.lookup("LS");
		}
		catch (RemoteException | NotBoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*
		
		/**
		Class1 converter = null;
		
		try
		{
			converter = new Class1();
			converter.convert_ekgMove3_to_DekomTex("C:\\Users\\mladenov\\Documents\\DataSets\\Integration\\Mesana_53C3CD8EA00F7", "C:\\Users\\mladenov\\Documents\\DataSets\\Integration\\Mesana_F7n");
		}
		catch (MWException e)
		{
			e.printStackTrace();
		}
		*/
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
	}
	
	
	public static void print(String msg)
	{
		System.out.println(msg);
	}
	
	

}
