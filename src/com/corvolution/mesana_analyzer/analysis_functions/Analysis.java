package com.corvolution.mesana_analyzer.analysis_functions;

import java.util.ArrayList;

import com.corvolution.mesana_analyzer.analysis_functions.AbstractAnalysis.State;
import com.corvolution.mesana_analyzer.data.HostServer;
/**
 * This interface provides all methods for managing a concrete analysis.
 * @author Deyan Mladenov
 *
 */
public interface Analysis
{

	/**
	 * This method returns a list of all Analysis classes that have to be executed before the current analysis.
	 * @return list of preconditions
	 */
	public ArrayList<Analysis> getPrecondition();

	/**
	 * This method starts an analysis. An analysis is executed on a remote server defined by the parameter host. 
	 * If the parameter host is not initialized (null) the analysis is executed locally.
	 * @param pathToMeasurement a full path to the folder, where the measurement data to analyze is located.
	 * @param host object containing all necessary credentials for a host
	 * @param overwrite a boolean prameeter, that defines whether all potential result data should be deleted, before starting the analysis
	 * @return true if analysis completed successfully, otherwise false
	 */
	public boolean start(String pathToMeasurement, HostServer host, boolean overwrite);

	@Deprecated
	public State getState();

	@Deprecated
	public void setMeasurementIdPath(String mId);
	
	@Deprecated
	public String getMeasurementIdPath();
	
	public void setOverwrite(boolean overwrite);
	
	@Override
	public boolean equals(Object arg0);

}
