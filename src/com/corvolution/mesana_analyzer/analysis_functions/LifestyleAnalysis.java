package com.corvolution.mesana_analyzer.analysis_functions;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;


import org.unisens.UnisensParseException;

import java.io.*;

import com.corvolution.mesana_analyzer.Configuration;
import com.corvolution.mesana_analyzer.data.HostServer;
import com.corvolution.mesana_analyzer.utility.LoggerConfigurator;

import mesana.dataanalyzer.LifestyleAnalysisRemote;

/**
 * This class manages the moviesens data analyzer, used for analyzing the lifestyle. The Lifestyle Analysis is provided as an executable.
 * @author Deyan Mladenov
 *
 */
public class LifestyleAnalysis extends AbstractAnalysis
{
	private static final String LS = "LS";
	
	//private static Logger logger;
	
	public LifestyleAnalysis()
	{
		precondition = new ArrayList<Analysis>();
		precondition.add(new QuestionnaireAnalysis());
		super.entryIdList = new ArrayList<String>(Arrays.asList("StepCount.csv", "AccActivityClassReport.csv"));
		super.customAttributesKeyList = new ArrayList<String>(Arrays.asList("revision"));
		super.filesList = new ArrayList<String>(Arrays.asList("stressreport.json","ActivityClass_1.png","ActivityClass_2.png","BaevskiiStressIndex_1.png","BaevskiiStressIndex_2.png",
				"HeartRate_1.png","HeartRate_2.png","HrvSpectrogram_1.png","HrvSpectrogram_2.png","LfHfBalance_1.png","LfHfBalance_2.png","StepCount_1.png","StepCount_2.png"));
		super.filesToMove = super.filesList;
	}

	@Override
	public boolean equals(Object arg0)
	{
		if (arg0 == null || !(arg0 instanceof LifestyleAnalysis))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	class StreamGobbler
	{
	    InputStream is;
	    String type;
	    OutputStream os;
	    
	    StreamGobbler(InputStream is, String type)
	    {
	        this(is, type, null);
	    }
	    StreamGobbler(InputStream is, String type, OutputStream redirect)
	    {
	        this.is = is;
	        this.type = type;
	        this.os = redirect;
	    }
	    
	    public void run()
	    {
	        try
	        {
	            PrintWriter pw = null;
	            if (os != null)
	                pw = new PrintWriter(os);
	                
	            InputStreamReader isr = new InputStreamReader(is);
	            BufferedReader br = new BufferedReader(isr);
	            String line=null;
	            while ( (line = br.readLine()) != null)
	            {
	                if (pw != null)
	                    pw.println(line);
	                System.out.println(type + ">" + line);    
	            }
	            if (pw != null)
	                pw.flush();
	        } catch (IOException ioe)
	            {
	            ioe.printStackTrace();  
	            }
	    }
	}

	@Override
	protected boolean executeLocally(String pathToMeasurement)
	{
		Runtime rt = Runtime.getRuntime();
		Process proc;
		
		String execCommand = "\"" + Configuration.getInstance().getDataAnalyzerPath() + "\" " +
				pathToMeasurement +
				" " + Configuration.getInstance().getDataAnalyzerParameters();
       
		try
		{ 
			FileOutputStream fos = new FileOutputStream(pathToMeasurement + "\\DataAnalyzerLog.txt");
			proc = rt.exec(execCommand);
			System.out.println("DataAnalyzer execution started. Waiting...");
			
			StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream(), "ERROR", fos);            
	            
	            // any output?
	        StreamGobbler outputGobbler = new StreamGobbler(proc.getInputStream(), "OUTPUT", fos);
	                
	            // kick them off
	     	errorGobbler.run();
	     	outputGobbler.run();

			int exitval = proc.waitFor();
			System.out.println("DataAnalyzer finished with exit val: " + exitval);
			fos.flush();
			fos.close();
			return true;
		}
		catch (IOException | InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		
		
	}

	@Override
	protected boolean executeRemotely(String pathToMeasurement, HostServer server)
	{
		String execCommand = "\"" + Configuration.getInstance().getDataAnalyzerPath() + "\" " +
				pathToMeasurement +
				" " + Configuration.getInstance().getDataAnalyzerParameters();
		
		Registry reg = server.getRegistry();
		
		try
		{
			LifestyleAnalysisRemote lsRemote = (LifestyleAnalysisRemote) reg.lookup(LS);
			lsRemote.execDataAnalyzer(execCommand, pathToMeasurement);
		}
		catch (RemoteException | NotBoundException e)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
			e.printStackTrace();
			return false;
		}
		return true;
	}


	@Override
	public boolean start(String pathToMeasurement, HostServer host, boolean overwrite)
	{
		logger = LoggerConfigurator.getErrorLogger();
		super.setMeasurementIdPath(pathToMeasurement);
		
		if (overwrite)
		{
			try
			{
				super.deleteData();
			}
			catch (UnisensParseException | IOException e)
			{
				logger.log(Level.SEVERE, "File deletion error in class {0}, analyzing {1} with exception {2}", new Object[]{this.getClass().getSimpleName(), pathToMeasurement, e.toString()});
				e.printStackTrace();
				return false;
			}
		}
		else
		{
			boolean filesComplete = false;
			try
			{
				filesComplete = super.checkResultDataExists();
			}
			catch (FileNotFoundException | UnisensParseException e)
			{
				logger.log(Level.SEVERE, "File deletion error in class {0}, analyzing {1} with exception {2}", new Object[]{this.getClass().getSimpleName(), pathToMeasurement, e.toString()});
				e.printStackTrace();
				return false;
			}
			if (filesComplete)
			{
				return true;
			}
			else
			{
				try
				{
					super.deleteData();
				}
				catch (UnisensParseException | IOException e)
				{
					logger.log(Level.SEVERE, "File deletion error in class {0}, analyzing {1} with exception {2}", new Object[]{this.getClass().getSimpleName(), pathToMeasurement, e.toString()});
					e.printStackTrace();
					return false;
				}
			}
		}
		
		boolean analysisSuccess = false;
		if (host == null)
		{
			 analysisSuccess = this.executeLocally(pathToMeasurement);
		}
		else
		{
			analysisSuccess = this.executeRemotely(pathToMeasurement, host);
		}
		
		if (!analysisSuccess)
		{
			logger.log(Level.SEVERE, "Analysis execution failed. See error log for details. Measurement: " + pathToMeasurement);
			return false;
		}
		
		String sourcePath = pathToMeasurement + "_result";
		try
		{
			super.moveDataToCurrent(sourcePath);
		}
		catch (IOException | UnisensParseException e1)
		{
			logger.log(Level.SEVERE, "Error occured when moving files from result file to main file. Measurement: " + pathToMeasurement, e1);
			e1.printStackTrace();
			return false;
		}
		
		boolean filesComplete = false;
		try
		{
			filesComplete = super.checkResultDataExists();
		}
		catch (FileNotFoundException | UnisensParseException e)
		{
			logger.log(Level.SEVERE, "File deletion error in class {0}, analyzing {1} with exception {2}", new Object[]{this.getClass().getSimpleName(), pathToMeasurement, e.toString()});
			e.printStackTrace();
			return false;
		}
		if (filesComplete)
		{
			return true;
		}
		else
		{
			logger.log(Level.SEVERE, "Result files incomplete after analysis. Measurement: " + pathToMeasurement);
			return false;
		}
		
		
	}
	

}
