package com.corvolution.mesana_analyzer.analysis_functions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.UndeclaredThrowableException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.unisens.UnisensParseException;

import com.corvolution.mesana_analyzer.Main;
import com.corvolution.mesana_analyzer.data.HostServer;
import com.corvolution.mesana_analyzer.utility.LoggerConfigurator;
import com.mathworks.toolbox.javabuilder.MWCharArray;
import com.mathworks.toolbox.javabuilder.MWException;

import afdetection.MatlabAfDetection;
import afdetection.MatlabAfDetectionRemote;

/**
 * This class manages the Atrial Fibliration analysis. The AF Analysis is implemented in Matlab and compiled to jar library, which is used in this class.
 * @author Deyan Mladenov
 *
 */
public class AfDetection extends AbstractAnalysis
{
	private static final String AF = "AF";

	
	private static Logger logger = LoggerConfigurator.getErrorLogger();
	
	/**
	 * Generate new instance of this class.
	 */
	public AfDetection()
	{
		precondition = new ArrayList<Analysis>();
		precondition.add(new MedicalAnalysis());
		super.customAttributesKeyList = new ArrayList<String>(Arrays.asList("afib.meanLength","afib.ratio","afib.meanHr","afib.maxLength","afib.maxHr","afib.number"));
		super.entryIdList = new ArrayList<String>(Arrays.asList("af_regions.csv","af_results.csv","af_duration.csv","af_regions_continuous.csv","af_regions_decision.csv"));
	}

	
	
	@Override
	public boolean equals(Object arg0)
	{
		if (arg0 == null || !(arg0 instanceof AfDetection))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	@Override
	protected boolean executeLocally(String pathToMeasurement)
	{
		URL cfgurl = Main.class.getClassLoader().getResource("config_examination.xlsx"); // here should check if getResource returns null
		if (cfgurl == null)
		{
			logger.log(Level.SEVERE, "Cannot find resource config_examination.xlsx for AfDetection");
			return false;
		}
		Path cfgpath = null;
		try
		{
			cfgpath = Paths.get(cfgurl.toURI());
		}
		catch (URISyntaxException e1)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e1);
			return false;
		}
		MWCharArray cfg = new MWCharArray(cfgpath.toString().toCharArray());
		MWCharArray pathtomeasurement = new MWCharArray(super.getMeasurementIdPath());
		MatlabAfDetection afanalysis = null;
		try
		{
			afanalysis = new MatlabAfDetection();
			afanalysis.examine_1_unisens_record(0, pathtomeasurement, cfg);
		}
		catch (MWException e)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
			return false;
		}
		catch (UndeclaredThrowableException e2)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e2.getUndeclaredThrowable());
			e2.printStackTrace();
			return false;
		}
		catch (Exception e3)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e3);
			e3.printStackTrace();
			return false;
		}
		finally 
		{
			cfg.dispose();
			pathtomeasurement.dispose();
			afanalysis.dispose();
		}
		return true;
	}

	@Override
	protected boolean executeRemotely(String pathToMeasurement, HostServer server)
	{
		logger.log(Level.FINER, "Trying to access registry. Measurement: "  + pathToMeasurement);
		Registry reg = server.getRegistry();
		MatlabAfDetectionRemote afremote = null;
		URL cfgurl = Main.class.getClassLoader().getResource("config_examination.xlsx"); // here should check if getResource returns null
		if (cfgurl == null)
		{
			logger.log(Level.SEVERE, "Cannot find resource config_examination.xlsx for AfDetection");
			return false;
		}
		Path cfgpath = null;
		MWCharArray cfg = null;
		try
		{	
			afremote = (MatlabAfDetectionRemote) reg.lookup(AF);
			logger.log(Level.FINEST, "AfRemote found. Measurement: " + pathToMeasurement);
			
			cfgpath = Paths.get(cfgurl.toURI());
			cfg = new MWCharArray(cfgpath.toString().toCharArray());
			//MWCharArray pathtomeasurement = new MWCharArray(super.getMeasurementIdPath());
			
			//MWCharArray input = new MWCharArray(super.getMeasurementIdPath() + ", " + cfgpath.toString().toCharArray());
			
			logger.log(Level.FINER, "Starting analysis on remote server. Measurement: " + pathToMeasurement);
			afremote.examine_1_unisens_record(0, pathToMeasurement, cfg);
			logger.log(Level.FINER, "Finished analysis on remote server: "  + pathToMeasurement);
		}
		catch (URISyntaxException | NotBoundException | RemoteException e)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
			e.printStackTrace();
			return false;
		}
		// This exception may be thrown, when an error of unknown to Matlab type occurs. For example a Java method used by the Matlab function throws an exception.
		catch (UndeclaredThrowableException e2)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e2.getUndeclaredThrowable());
			e2.printStackTrace();
			return false;
		}
		catch (Exception e3)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e3);
			e3.printStackTrace();
			return false;
		}
		finally
		{
			cfg.dispose();
		}
		return true;
	}



	@Override
	public boolean start(String pathToMeasurement, HostServer host, boolean overwrite)
	{
		
		logger = LoggerConfigurator.getErrorLogger();
		super.setMeasurementIdPath(pathToMeasurement);
		
		if (overwrite)
		{
			try
			{
				super.deleteData();
			}
			catch (UnisensParseException | IOException e)
			{
				logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
				e.printStackTrace();
				return false;
			}
		}
		else
		{
			boolean filesComplete = false;
			try
			{
				filesComplete = super.checkResultDataExists();
			}
			catch (FileNotFoundException | UnisensParseException e)
			{
				logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
				e.printStackTrace();
				return false;
			}
			if (filesComplete)
			{
				return true;
			}
			else
			{
				try
				{
					super.deleteData();
				}
				catch (UnisensParseException | IOException e)
				{
					logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
					e.printStackTrace();
					return false;
				}
			}
		}
		
		
		boolean analysisSuccess = false;
		
		if (host == null)
		{
			analysisSuccess = this.executeLocally(pathToMeasurement);
		}
		else
		{
			analysisSuccess = this.executeRemotely(pathToMeasurement, host);
		}
		
		if (!analysisSuccess)
		{
			logger.log(Level.SEVERE, "Analysis execution failed. See error log for details. Measurement: " + pathToMeasurement);
			return false;
		}
		
		boolean filesComplete = false;
		try
		{
			filesComplete = super.checkResultDataExists();
		}
		catch (FileNotFoundException | UnisensParseException e)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
			e.printStackTrace();
			return false;
		}
		if (filesComplete)
		{
			return true;
		}
		else
		{
			logger.log(Level.SEVERE, "Result files incomplete after analysis. Measurement: " + pathToMeasurement);
			return false;
		}
		
		
	}


	



	
	





	
	

	

}
