package com.corvolution.mesana_analyzer.analysis_functions;


import java.lang.reflect.UndeclaredThrowableException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;


import com.corvolution.mesana_analyzer.data.HostServer;
import com.mathworks.toolbox.javabuilder.MWException;

import convert_ekgMove3_to_DekomTex.Class1;
import convert_ekgMove3_to_DekomTex.Class1Remote;

public class DataConverter extends AbstractAnalysis
{
	private static final String DC = "DC";

	public DataConverter()
	{
		super.entryIdList = new ArrayList<String>(Arrays.asList("acc3ch.bin","ecg3ch.bin","temp_caremon.bin"));
		
	}

	@Override
	public boolean start(String pathToMeasurement, HostServer host, boolean overwrite)
	{
		return true;
		/**
		logger = LoggerConfigurator.getErrorLogger();
		super.setMeasurementIdPath(pathToMeasurement);
		super.setOverwrite(overwrite);
		
		if (overwrite)
		{
			try
			{
				super.deleteData();
			}
			catch (UnisensParseException | IOException e)
			{
				logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
				e.printStackTrace();
				return false;
			}
		}
		else
		{
			boolean filesComplete = false;
			try
			{
				filesComplete = super.checkResultDataExists();
			}
			catch (FileNotFoundException | UnisensParseException e)
			{
				logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
				e.printStackTrace();
				return false;
			}
			if (filesComplete)
			{
				return true;
			}
			else
			{
				try
				{
					super.deleteData();
				}
				catch (UnisensParseException | IOException e)
				{
					logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
					e.printStackTrace();
					return false;
				}
			}
		}
		
		
		boolean analysisSuccess = false;
		
		//Out of memory error when starting multiple instances of converter.
		analysisSuccess = this.executeLocally(pathToMeasurement);
		
		
		
		
		if (!analysisSuccess)
		{
			logger.log(Level.SEVERE, "Analysis execution failed. See error log for details. Measurement: " + pathToMeasurement);
			return false;
		}
		
		boolean filesComplete = false;
		try
		{
			filesComplete = super.checkResultDataExists();
		}
		catch (FileNotFoundException | UnisensParseException e)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
			e.printStackTrace();
			return false;
		}
		if (filesComplete)
		{
			return true;
		}
		else
		{
			logger.log(Level.SEVERE, "Result files incomplete after analysis. Measurement: " + pathToMeasurement);
			return false;
		}
		*/
	}

	@Override
	protected synchronized boolean executeLocally(String pathToMeasurement)
	{
		Class1 converter = null;
		
		try
		{
			converter = new Class1();
			converter.convert_ekgMove3_to_DekomTex(pathToMeasurement);
		}
		catch (MWException e)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
			e.printStackTrace();
			return false;
		}
		catch (UndeclaredThrowableException e2)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e2.getUndeclaredThrowable());
			e2.printStackTrace();
			return false;
		}
		catch (Exception e3)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e3);
			e3.printStackTrace();
			return false;
		}
		finally
		{
			converter.dispose();
		}
		
		return true;
	}

	@Override
	protected boolean executeRemotely(String pathToMeasurement, HostServer server)
	{
		Registry reg = server.getRegistry();
		Class1Remote converter = null;
		try
		{
			converter = (Class1Remote) reg.lookup(DC);
			converter.convert_ekgMove3_to_DekomTex(pathToMeasurement);
			
		}
		catch (RemoteException | NotBoundException e1)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e1);
			e1.printStackTrace();
			return false;
		}
		catch (UndeclaredThrowableException e2)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e2.getUndeclaredThrowable());
			return false;

		}
		catch (Exception e3)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e3);
			return false;
		}
		return true;
	}
	
	@Override
	public boolean equals(Object arg0)
	{
		if (arg0 == null || !(arg0 instanceof DataConverter))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	

}
