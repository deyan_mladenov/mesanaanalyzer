package com.corvolution.mesana_analyzer.analysis_functions;

import java.util.ArrayList;

import com.corvolution.mesana_analyzer.data.HostServer;

/**
 * This class manages the generation of a recommendation. The Recommendation Generator is implemented in ????.
 * @author Deyan Mladenov
 *
 */
public class RecommendationGenerator extends AbstractAnalysis
{

	public RecommendationGenerator()
	{
		super();
		precondition = new ArrayList<Analysis>();
		this.precondition.add(new LifestyleAnalysis());
	}

	
	public void startold()
	{
		System.out.println("this is recommendation generator");
		// TODO Auto-generated method stub

	}

	@Override
	public State getState()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object arg0)
	{
		if (arg0 == null || !(arg0 instanceof RecommendationGenerator))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	@Override
	protected boolean executeLocally(String pathToMeasurement)
	{
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	protected boolean executeRemotely(String pathToMeasurement, HostServer server)
	{
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean start(String pathToMeasurement, HostServer host, boolean overwrite)
	{
		// TODO Auto-generated method stub
		return true;
	}

	

}
