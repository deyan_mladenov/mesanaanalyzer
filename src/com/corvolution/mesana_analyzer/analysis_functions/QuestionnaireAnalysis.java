package com.corvolution.mesana_analyzer.analysis_functions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.UndeclaredThrowableException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;

import org.unisens.UnisensParseException;

import com.corvolution.mesana_analyzer.data.HostServer;
import com.corvolution.mesana_analyzer.utility.LoggerConfigurator;
import com.mathworks.toolbox.javabuilder.MWCharArray;
import com.mathworks.toolbox.javabuilder.MWException;

import mmsqa.QuestionnaireAnalysisImplementation;
import mmsqa.QuestionnaireAnalysisImplementationRemote;

/**
 * This class manages the analysis of a questionnaire. The Questionnaire Analysis is implemented in Matlab and compiled to jar library, which is used in this class.
 * @author Deyan Mladenov
 *
 */
public class QuestionnaireAnalysis extends AbstractAnalysis
{

	private static final String QA = "QA";
	
	//private static Logger logger;
	
	public QuestionnaireAnalysis()
	{
		super.precondition = new ArrayList<Analysis>();
		super.precondition.add(new MedicalAnalysis());
		super.filesList = new ArrayList<String>(Arrays.asList("questionaire_data.json", "questionnaire_mesana.json", "results_procam.json"));
		super.customAttributesKeyList = new ArrayList<String>(Arrays.asList("gender","age","weight","height", "stressFeeling", "stressSymptoms"));
		logger = LoggerConfigurator.getErrorLogger();
	}

	
	public void startold()
	{
		System.out.println("this is questionnaire analysis");
		super.setState(State.RUNNING);
		
		MWCharArray pathtomeasurement = new MWCharArray(super.getMeasurementIdPath());
		QuestionnaireAnalysisImplementation qaImpl = null;
		try
		{
			qaImpl = new QuestionnaireAnalysisImplementation();
			qaImpl.rp1_queastionaire_analysis(pathtomeasurement);
		}
		catch (MWException e)
		{
			setState(State.FAILURE);
			return;
		}
		finally
		{
			pathtomeasurement.dispose();
			qaImpl.dispose();
		}
		
		super.setState(State.FINISHED);
	}

	@Override
	public boolean equals(Object arg0)
	{
		if (arg0 == null || !(arg0 instanceof QuestionnaireAnalysis))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	@Override
	protected boolean executeLocally(String pathToMeasurement)
	{
		QuestionnaireAnalysisImplementation qa = null;
		
		try
		{
			qa = new QuestionnaireAnalysisImplementation();
			qa.rp1_queastionaire_analysis(pathToMeasurement);
		}
		catch (MWException e)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
			e.printStackTrace();
			return false;
		}
		catch (UndeclaredThrowableException e2)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e2.getUndeclaredThrowable());
			e2.printStackTrace();
			return false;
		}
		catch (Exception e3)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e3);
			e3.printStackTrace();
			return false;
		}
		finally
		{
			qa.dispose();
		}
		
		return true;
	}

	@Override
	protected boolean executeRemotely(String pathToMeasurement, HostServer server)
	{
		logger.log(Level.FINE, "Trying to access registry");
		Registry reg = server.getRegistry();
		QuestionnaireAnalysisImplementationRemote qaRemote = null;
		
		try
		{
			qaRemote = (QuestionnaireAnalysisImplementationRemote) reg.lookup(QA);
			qaRemote.rp1_queastionaire_analysis(pathToMeasurement);
		}
		catch (RemoteException | NotBoundException e1)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e1);
			e1.printStackTrace();
			return false;
		}
		catch (UndeclaredThrowableException e2)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e2.getUndeclaredThrowable());
			e2.printStackTrace();
			return false;
		}
		catch (Exception e3)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e3);
			e3.printStackTrace();
			return false;
		}
		return true;
	}


	@Override
	public boolean start(String pathToMeasurement, HostServer host, boolean overwrite)
	{
		logger = LoggerConfigurator.getErrorLogger();
		super.setMeasurementIdPath(pathToMeasurement);
		
		if (overwrite)
		{
			try
			{
				super.deleteData();
			}
			catch (UnisensParseException | IOException e)
			{
				logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
				e.printStackTrace();
				return false;
			}
		}
		else
		{
			boolean filesComplete = false;
			try
			{
				filesComplete = super.checkResultDataExists();
			}
			catch (FileNotFoundException | UnisensParseException e)
			{
				logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
				e.printStackTrace();
				return false;
			}
			if (filesComplete)
			{
				return true;
			}
			else
			{
				try
				{
					super.deleteData();
				}
				catch (UnisensParseException | IOException e)
				{
					logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
					e.printStackTrace();
					return false;
				}
			}
		}
		
		
		boolean analysisSuccess = false;
		
		if (host == null)
		{
			analysisSuccess = this.executeLocally(pathToMeasurement);
		}
		else
		{
			analysisSuccess = this.executeRemotely(pathToMeasurement, host);
		}
		
		
		if (!analysisSuccess)
		{
			logger.log(Level.SEVERE, "Analysis execution failed. See error log for details. Measurement: " + pathToMeasurement);
			return false;
		}
		
		boolean filesComplete = false;
		try
		{
			filesComplete = super.checkResultDataExists();
		}
		catch (FileNotFoundException | UnisensParseException e)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
			e.printStackTrace();
			return false;
		}
		if (filesComplete)
		{
			return true;
		}
		else
		{
			logger.log(Level.SEVERE, "Result files incomplete after analysis. Measurement: " + pathToMeasurement);
			return false;
		}
	}




}
