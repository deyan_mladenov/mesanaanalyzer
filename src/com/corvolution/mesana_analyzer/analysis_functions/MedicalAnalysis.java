package com.corvolution.mesana_analyzer.analysis_functions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.UndeclaredThrowableException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;

import org.unisens.UnisensParseException;

import com.corvolution.mesana_analyzer.data.HostServer;
import com.corvolution.mesana_analyzer.utility.LoggerConfigurator;
import com.mathworks.toolbox.javabuilder.MWException;


import dtr.MedicalAnalysisImplementation;
import dtr.MedicalAnalysisImplementationRemote;


/**
 * This class manages the (DTR) Medical analysis. The Medical Analysis is implemented in Matlab and compiled to jar
 * library, which is used in this class.
 * 
 * @author Deyan Mladenov
 *
 */
public class MedicalAnalysis extends AbstractAnalysis
{
	private static final String DTR = "DTR";
	

	/**
	 * Generate instance of this class.
	 */
	public MedicalAnalysis()
	{
		super.precondition = new ArrayList<Analysis>();
		super.precondition.add(new DataConverter());
		super.filesList = new ArrayList<String>(Arrays.asList("ecg_ampl_rpeak_baseline_ch1.csv", "ecg_ampl_rpeak_baseline_ch2.csv", "ecg_ampl_rpeak_baseline_ch3.csv",
				"ecg_ampl_rpeak_baseline_ch4.csv", "ecg_ampl_rpeak_baseline_ch5.csv", "ecg_amplitude.bin", "ecg_artifact.csv", "ecg_artifact_ch0.csv", "ecg_artifact_ch1.csv", "ecg_artifact_ch2.csv",
				"ecg_artifact_ch3.csv", "ecg_artifact_ch4.csv", "ecg_artifact_ch5.csv", "ecg_artifact_val.csv", "ecg_artifact_values_ch0.csv", "ecg_artifact_values_ch1.csv", "ecg_artifact_values_ch2.csv",
				"ecg_artifact_values_ch3.csv", "ecg_artifact_values_ch4.csv", "ecg_artifact_values_ch5.csv", "ecg_artifact2.csv", "ecg_artifact2_val.csv", "ecg_artifacts_extern.bin", "ecg_artifacts_extern2.bin",
				"ecg_artifacts_movement.bin", "ecg_quality_param_ch0.csv", "ecg_quality_param_ch1.csv", "ecg_quality_param_ch2.csv", "ecg_quality_param_ch3.csv", "ecg_quality_param_ch4.csv",
				"ecg_quality_param_ch5.csv", "ecg_quality_rating_ch0.csv", "ecg_quality_rating_ch1.csv", "ecg_quality_rating_ch2.csv", "ecg_quality_rating_ch3.csv", "ecg_quality_rating_ch4.csv",
				"ecg_quality_rating_ch5.csv", "ecg_rpeak_delays.csv", "ecg_trigger.csv", "ecg_trigger_ch0.csv", "ecg_trigger_ch1.csv", "ecg_trigger_ch2.csv", "ecg_trigger_ch3.csv", "ecg_trigger_ch4.csv",
				"ecg_trigger_ch5.csv", "ecg_trigger_osea_ch0.csv", "ecg_trigger_osea_ch1.csv", "ecg_trigger_osea_ch2.csv", "ecg_trigger_osea_ch3.csv", "ecg_trigger_osea_ch4.csv", "ecg_trigger_osea_ch5.csv", 
				"ecg_trigger2.csv", "ecg_trigger2_rhythm1_N.csv", "ecg_trigger2_rhythm2_S.csv", "ecg_trigger2_rhythm3_T.csv", "ecg_trigger2_rhythm4_B.csv", "ecg_trigger2_rhythm5_P.csv",
				"ecg_trigger2_rhythm6_V.csv", "ecg_trigger2_rhythm7_VT.csv", "ecg_trigger2_rhythm8_CPL.csv", "ecg_trigger2_rhythm9_TPL.csv", "ecg_trigger2_rhythm10_SLV.csv", "ecg_trigger2_rhythm11_A.csv",
				"ecg_trigger2_rhythm12_Q.csv", "ecg3chadd.bin", "ecg6ch.bin", "fall.csv", "heartrate_avg.bin", "heartrate_curr.bin", "movement.bin", "pause2.csv",
				"position.bin", "rhythms2.bin", "standup.csv", "tachogram2.csv"));
		super.entryIdList = new ArrayList<String>(Arrays.asList("ecg_ampl_rpeak_baseline_ch1.csv", "ecg_ampl_rpeak_baseline_ch2.csv", "ecg_ampl_rpeak_baseline_ch3.csv",
				"ecg_ampl_rpeak_baseline_ch4.csv", "ecg_ampl_rpeak_baseline_ch5.csv", "ecg_amplitude.bin", "ecg_artifact.csv", "ecg_artifact_ch0.csv", "ecg_artifact_ch1.csv", "ecg_artifact_ch2.csv",
				"ecg_artifact_ch3.csv", "ecg_artifact_ch4.csv", "ecg_artifact_ch5.csv", "ecg_artifact_val.csv", "ecg_artifact_values_ch0.csv", "ecg_artifact_values_ch1.csv", "ecg_artifact_values_ch2.csv",
				"ecg_artifact_values_ch3.csv", "ecg_artifact_values_ch4.csv", "ecg_artifact_values_ch5.csv", "ecg_artifact2.csv", "ecg_artifact2_val.csv", "ecg_artifacts_extern.bin", "ecg_artifacts_extern2.bin",
				"ecg_artifacts_movement.bin", "ecg_quality_param_ch0.csv", "ecg_quality_param_ch1.csv", "ecg_quality_param_ch2.csv", "ecg_quality_param_ch3.csv", "ecg_quality_param_ch4.csv",
				"ecg_quality_param_ch5.csv", "ecg_quality_rating_ch0.csv", "ecg_quality_rating_ch1.csv", "ecg_quality_rating_ch2.csv", "ecg_quality_rating_ch3.csv", "ecg_quality_rating_ch4.csv",
				"ecg_quality_rating_ch5.csv", "ecg_rpeak_delays.csv", "ecg_trigger.csv", "ecg_trigger_ch0.csv", "ecg_trigger_ch1.csv", "ecg_trigger_ch2.csv", "ecg_trigger_ch3.csv", "ecg_trigger_ch4.csv",
				"ecg_trigger_ch5.csv", "ecg_trigger_osea_ch0.csv", "ecg_trigger_osea_ch1.csv", "ecg_trigger_osea_ch2.csv", "ecg_trigger_osea_ch3.csv", "ecg_trigger_osea_ch4.csv", "ecg_trigger_osea_ch5.csv", 
				"ecg_trigger2.csv", "ecg_trigger2_rhythm1_N.csv", "ecg_trigger2_rhythm2_S.csv", "ecg_trigger2_rhythm3_T.csv", "ecg_trigger2_rhythm4_B.csv", "ecg_trigger2_rhythm5_P.csv",
				"ecg_trigger2_rhythm6_V.csv", "ecg_trigger2_rhythm7_VT.csv", "ecg_trigger2_rhythm8_CPL.csv", "ecg_trigger2_rhythm9_TPL.csv", "ecg_trigger2_rhythm10_SLV.csv", "ecg_trigger2_rhythm11_A.csv",
				"ecg_trigger2_rhythm12_Q.csv", "ecg3chadd.bin", "ecg6ch.bin", "fall.csv", "heartrate_avg.bin", "heartrate_curr.bin", "movement.bin", "pause2.csv",
				"position.bin", "rhythms2.bin", "standup.csv", "tachogram2.csv"));
	}


	
	@Override
	public boolean equals(Object arg0)
	{
		if (arg0 == null || !(arg0 instanceof MedicalAnalysis))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	@Override
	protected boolean executeLocally(String pathToMeasurement)
	{
		System.out.println("Starting Medical Analysis");
		
		MedicalAnalysisImplementation dtr = null;
		try
		{
			dtr = new MedicalAnalysisImplementation();
			dtr.dtr(pathToMeasurement);
		}
		catch (MWException e)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
			e.printStackTrace();
			return false;
		}
		catch (UndeclaredThrowableException e2)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e2.getUndeclaredThrowable());
			e2.printStackTrace();
			return false;
		}
		catch (Exception e3)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e3);
			e3.printStackTrace();
			return false;
		}
		finally
		{
			dtr.dispose();
		}
		
		return true;
	}


	@Override
	protected boolean executeRemotely(String pathToMeasurement, HostServer server)
	{
		logger.log(Level.FINE, "Trying to access registry");
		Registry reg = server.getRegistry();
		MedicalAnalysisImplementationRemote dtrRemote = null;
		
		try
		{
			dtrRemote = (MedicalAnalysisImplementationRemote) reg.lookup(DTR);
			if (super.isOverwrite())
			{
				//String[] input = {pathToMeasurement, "overwrite"};
				dtrRemote.dtr(pathToMeasurement, "overwrite");
			}
			else
			{
				dtrRemote.dtr(pathToMeasurement);
			}
			
		}
		catch (RemoteException | NotBoundException e)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
			e.printStackTrace();
			return false;
		}
		// This exception may be thrown, when an error of unknown to Matlab type occurs. For example a Java method used by the Matlab function throws an exception.
		catch (UndeclaredThrowableException e2)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e2.getUndeclaredThrowable());
			e2.printStackTrace();
			return false;
		}
		catch (Exception e3)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e3);
			e3.printStackTrace();
			return false;
		}
		return true;
	}



	@Override
	public boolean start(String pathToMeasurement, HostServer host, boolean overwrite)
	{
		logger = LoggerConfigurator.getErrorLogger();
		super.setMeasurementIdPath(pathToMeasurement);
		super.setOverwrite(overwrite);
		
		if (overwrite)
		{
			try
			{
				super.deleteData();
			}
			catch (UnisensParseException | IOException e)
			{
				logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
				e.printStackTrace();
				return false;
			}
		}
		else
		{
			boolean filesComplete = false;
			try
			{
				filesComplete = super.checkResultDataExists();
			}
			catch (FileNotFoundException | UnisensParseException e)
			{
				logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
				e.printStackTrace();
				return false;
			}
			if (filesComplete)
			{
				return true;
			}
			else
			{
				try
				{
					super.deleteData();
				}
				catch (UnisensParseException | IOException e)
				{
					logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
					e.printStackTrace();
					return false;
				}
			}
		}
		
		
		boolean analysisSuccess = false;
		
		if (host == null)
		{
			analysisSuccess = this.executeLocally(pathToMeasurement);
			
		}
		else
		{
			analysisSuccess = this.executeRemotely(pathToMeasurement, host);
		}
		
		if (!analysisSuccess)
		{
			logger.log(Level.SEVERE, "Analysis execution failed. See error log for details. Measurement: " + pathToMeasurement);
			return false;
		}
		
		boolean filesComplete = false;
		try
		{
			filesComplete = super.checkResultDataExists();
		}
		catch (FileNotFoundException | UnisensParseException e)
		{
			logger.log(Level.SEVERE, "Exception in class " + this.getClass().getSimpleName() + ", analyzing " + pathToMeasurement , e);
			e.printStackTrace();
			return false;
		}
		if (filesComplete)
		{
			return true;
		}
		else
		{
			logger.log(Level.SEVERE, "Result files incomplete after analysis. Measurement: " + pathToMeasurement);
			return false;
		}
	}
		

}
