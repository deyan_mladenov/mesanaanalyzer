package com.corvolution.mesana_analyzer.analysis_functions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystemException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonStructure;
import javax.json.JsonValue;

import static java.nio.file.StandardCopyOption.*;

import org.apache.commons.io.FileUtils;
import org.unisens.*;

import com.corvolution.mesana_analyzer.data.HostServer;

/**
 * This is a class abstraction of all analysis classes. 
 * This class contains attributes and methods that are necessary for the function of all analysis classes.
 * 
 * @author Deyan Mladenov
 *
 */
public abstract class AbstractAnalysis implements Analysis
{
	protected static Logger logger;
	
	/**
	 *  Store the analyzes that need to be executed before this analysis runs.
	 */
	protected ArrayList<Analysis> precondition = null;

	/**
	 *  Path to measurement
	 */
	private String measurementIdPath;

	/**
	 *  The state of this Analysis as defined in Enum State.
	 */
	private State state;
	
	private boolean overwrite;
	
	/**
	 *  List of IDs of entries in the unisens file, that are expected to be created after running the concrete analysis function.
	 */
	protected ArrayList<String> entryIdList = null;
	
	/**
	 *  List if custom attributes in an unisens file, that are expected to be created after running the concrete analysis function.
	 */
	protected ArrayList<String> customAttributesKeyList = null;
	
	/**
	 *  List of files in the measurement folder, that are expected to be created after running the concrete analysis function.
	 */
	protected ArrayList<String> filesList = null;
	
	/**
	 *  List of files that are going to be moved from another folder in this mesurement's folder.
	 *  Currently this is the case only after the Lifestyle Analysis.
	 */
	protected ArrayList<String> filesToMove = null;
	
	/**
	 * List of IDs in the measurement.json file that are expected to be created after running the concrete analysis function.
	 */
	protected ArrayList<String> measurementJsonIdList = null;
	
	/**
	 * States of the execution of a concrete analysis.
	 * This is a deprecated piece of code, not currently in use.
	 * @author Deyan Mladenov
	 *
	 */
	enum State
	{
		RUNNING, FINISHED, FAILURE

	}

	/**
	 * Getter, that returns the precondition list.
	 */
	public ArrayList<Analysis> getPrecondition()
	{
		return precondition;
	}

	/**
	 * Getter, that returns the current state.
	 */
	public State getState()
	{
		return this.state;
	}

	/**
	 * Set the new state.
	 */
	protected void setState(State newState)
	{
		this.state = newState;
	}

	/**
	 * Getter, that returns the path to concrete measurement.
	 */
	public String getMeasurementIdPath()
	{
		return measurementIdPath;
	}

	/**
	 * Set new path to measurement.
	 */
	public void setMeasurementIdPath(String measurementIdPath)
	{
		this.measurementIdPath = measurementIdPath;
	}
	
	public void setOverwrite(boolean overwrite)
	{
		this.overwrite = overwrite;
	}
	

	public boolean isOverwrite()
	{
		return overwrite;
	}

	public ArrayList<String> getEntryIdList()
	{
		return entryIdList;
	}

	public ArrayList<String> getCustomAttributesKeyList()
	{
		return customAttributesKeyList;
	}

	public ArrayList<String> getFilesList()
	{
		return filesList;
	}

	public ArrayList<String> getFilesToMove()
	{
		return filesToMove;
	}

	public ArrayList<String> getMeasurementJsonIdList()
	{
		return measurementJsonIdList;
	}
	
	protected abstract boolean executeLocally(String pathToMeasurement);
	
	protected abstract boolean executeRemotely(String pathToMeasurement, HostServer server);

	/**
	 * Check if the data is complete, according to the lists.
	 * For unisens.xml file the existence of all entries and custom attributes is checked.
	 * The existence of other files not listed in unisens.xml is checked.
	 * Measurement.json file is checked whether it is containing the listed IDs.
	 * @return true if all checks return true
	 * @throws UnisensParseException
	 * @throws FileNotFoundException
	 */
	protected boolean checkResultDataExists() throws UnisensParseException, FileNotFoundException
	{
		UnisensFactory uf = UnisensFactoryBuilder.createFactory();
		Unisens u = uf.createUnisens(measurementIdPath);
		boolean result = true;
		
		if (this.customAttributesKeyList != null)
		{
			
			result &= this.checkAttributes(u);
		}
		
		//Check that all files exist, no matter if they are listed in unisens.xml or not. 
		if (this.filesList != null && result != false)
		{
			result &= this.checkFiles();
		}
		
		if (this.entryIdList != null && result != false)
		{
			// TODO Log warning files are not present as entries in unisens.xml
		}
		
		if (this.measurementJsonIdList != null && result != false) 
		{
			result &= this.checkMeasurementJson();
		}
		return result;
	}
	
	/**
	 * This method deletes files and their representing entries in unisens.xml, as well as the custom attributes in the unisens.xml.
	 * @throws UnisensParseException
	 * @throws IOException
	 */
	protected void deleteData() throws UnisensParseException, IOException
	{
		UnisensFactory uf = UnisensFactoryBuilder.createFactory();
		Unisens u = uf.createUnisens(measurementIdPath);
		
		if (this.entryIdList != null) 
		{
			this.deleteEntries(u);
		}
		
		if (this.customAttributesKeyList != null)
		{
			this.deleteAttributes(u);
		}
		
		if (this.filesList != null) 
		{
			this.deleteFiles();
		}
		
		u.save();
	}
	
	/**
	 * This method moves data from a given path to the path of the current measurement.
	 * Important: The source path is expected to have a unisens.xml file.
	 * @param sourcePath
	 * @throws IOException
	 * @throws UnisensParseException
	 * @throws DuplicateIdException
	 */
	protected void moveDataToCurrent (String sourcePath) throws IOException, UnisensParseException
	{
		if (this.filesToMove != null)
		{
			this.moveFiles(sourcePath);
		}
		
		this.moveUnisensEntries(sourcePath);
		
		FileUtils.deleteDirectory(new File(sourcePath));

	}
	
	/**
	 * Check if the entries listed in the entryIdList are contained in the given unisens file.
	 * @param u class representing the unisens.xml
	 * @return true if all entries are contained
	 */
	public boolean checkEntries(Unisens u)
	{
		
		ArrayList<Entry> list = (ArrayList<Entry>) u.getEntries();
		ArrayList<String> unisensIdList = new ArrayList<String>();
		for (int i = 0; i < list.size(); i++)
		{
			unisensIdList.add(list.get(i).getId());
		}
		for (int i = 0; i < entryIdList.size(); i++) 
		{
			if (!unisensIdList.contains(entryIdList.get(i))) 
			{
				logger.log(Level.WARNING, "Entry " + entryIdList.get(i).toString() + " missing in measurement: " + this.measurementIdPath);
				return false;
			}
		}
		return true;

	}
	
	/**
	 * Check if the attributes listed in the customAttributesKeyList are contained in the given unisens file.
	 * @param u class representing the unisens.xml
	 * @return true if all custom attributes are contained
	 */
	public boolean checkAttributes(Unisens u)
	{
		Set<String> list = u.getCustomAttributes().keySet();
		Iterator<String> iter = this.customAttributesKeyList.iterator();
		while (iter.hasNext())
		{
			String current = iter.next();
			if (!list.contains(current))
			{
				logger.log(Level.WARNING, "Attribute " + current.toString() + " missing in measurement: " + this.measurementIdPath);
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Check if all files as given in the filesList are existing.
	 * @return true if files exist
	 */
	public boolean checkFiles()
	{
		for (int i = 0; i < this.filesList.size(); i++)
		{
			Path path = Paths.get(measurementIdPath, filesList.get(i));// CRITICAL test this
			if (!Files.exists(path))
			{
				logger.log(Level.WARNING, "File " + path.toString() + " missing in measurement: " + this.measurementIdPath);
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Measurement.json file is checked whether it is containing the IDs listed in measurementJsonIdList.
	 * @return true if all IDs are contained
	 * @throws FileNotFoundException
	 */
	public boolean checkMeasurementJson()
	{
		JsonReader jreader = null;
		try
		{
			jreader = Json.createReader(new FileReader(this.measurementIdPath + "\\measurement.json"));
		}
		catch (FileNotFoundException e)
		{
			logger.log(Level.SEVERE, "Measurement.json file was not found in: " + this.measurementIdPath, e);
			return false;
		}
		JsonObject jobject = jreader.readObject();
		for (int i = 0; i < this.measurementJsonIdList.size(); i++)
		{
			String IdValue = "\"" + this.measurementJsonIdList.get(i) + "\"";
			if (!this.jsonContainsValue(jobject, IdValue))
			{
				logger.log(Level.WARNING, "Measurement.json ID " + IdValue + " missing in measurement: " + this.measurementIdPath);
				return false;
			}
		}
		return true;
	}
	
	/**
	 * All entries as listed in the entryIdList are deleted from current unisens.xml.
	 * Please note that the corresponding files are deleted too.
	 * @param u
	 */
	public void deleteEntries(Unisens u)
	{
		List<Entry> list = u.getEntries();
		
		for (int i = list.size()-1; i >= 0; i--) 
		{
			if (entryIdList.contains(list.get(i).getId())) 
			{
				u.deleteEntry(list.get(i));
			}
		}
	}
	
	/**
	 * Delete all attributes as given in customAttributesKeyList.
	 * @param u Representation of the unisens.xml file, where the attributes are to be deleted.
	 */
	public void deleteAttributes(Unisens u)
	{
		HashMap<String,String> attributesList = u.getCustomAttributes();
		for (int i = 0; i < this.customAttributesKeyList.size(); i++)
		{
			attributesList.remove(customAttributesKeyList.get(i));
		}
	}
	
	/**
	 * Delete all files as given in the filesList.
	 * @throws IOException
	 */
	public void deleteFiles() throws IOException
	{
		for (int i = 0; i < this.filesList.size(); i++)
		{
			Path path = Paths.get(measurementIdPath, "\\" + filesList.get(i));
			try
			{
				Files.deleteIfExists(path);
				//File file = new File(measurementIdPath, filesList.get(i));
				//FileDeleteStrategy.FORCE.delete(file);
			}
			catch (FileSystemException e1)
			{
				logger.log(Level.WARNING, "FileSystemException thrown when trying to delete file:" + filesList.get(i) + "in measurement: " + this.measurementIdPath);
			}
			catch (IOException e2)
			{
				logger.log(Level.SEVERE, "IOException thrown  when trying to delete file:" + filesList.get(i) + "in measurement: " + this.measurementIdPath, e2);
			}
		
		}
	}
	
	/**
	 * Search json file for given string key value.
	 * @param jstruct Representation of the json file.
	 * @param value The key string to be searched for.
	 * @return true if the key value is found.
	 */
	public boolean jsonContainsValue(JsonStructure jstruct, String value)
	{
		if (jstruct instanceof JsonObject)
		{
			JsonObject jobject = (JsonObject) jstruct;
			if (jobject.containsKey("id") && jobject.get("id").toString().equals(value))
			{
				return true;
			}
			else
			{	
				for (String key : jobject.keySet())
				{
					JsonValue keyvalue = jobject.get(key);
					if (keyvalue instanceof JsonStructure)
					{
						if (jsonContainsValue((JsonStructure)keyvalue, value))
						{
							return true;
						}
						else {
							continue;
						}
					}
				}
				return false;
			}
		}
		else if (jstruct instanceof JsonArray)
		{
			JsonArray jarray = (JsonArray) jstruct;
			for (int i = 0; i < jarray.size(); i++)
			{
				JsonValue arrayentry = jarray.get(i);
				if (arrayentry instanceof JsonStructure)
				{
					if (jsonContainsValue((JsonStructure)arrayentry, value))
					{
						return true;
					}
					else
					{
						continue;
					}
				}
			}
			return false;
		}
		else 
		{
			return false;
		}
	}
	
	/**
	 * Move files as listed in filesToMove from source path to current path of measurement as destination.
	 * @param sourcepath
	 * @throws IOException
	 */
	public void moveFiles(String sourcepath) throws IOException
	{
		Path source = null;
		Path target = null;
		for (int i = 0; i < this.filesToMove.size(); i++)
		{
			source = Paths.get(sourcepath, this.filesToMove.get(i)); //CRITICAL
			target = Paths.get(this.measurementIdPath, this.filesToMove.get(i));
			Files.move(source, target, REPLACE_EXISTING);
		}
	}
	
	/**
	 * Move all unisens entries with their corresponding files from source unisens.xml to current unisens.xml.
	 * @param sourcepath The path of the source unisens.xml file.
	 * @throws UnisensParseException
	 * @throws IOException
	 */
	public void moveUnisensEntries(String sourcepath) throws UnisensParseException, IOException
	{
		UnisensFactory uf = UnisensFactoryBuilder.createFactory();
		Unisens uTarget = uf.createUnisens(measurementIdPath);
		Unisens uSource = uf.createUnisens(sourcepath);
		ArrayList<Entry> entries = (ArrayList<Entry>) uSource.getEntries();
		for (int i = 0; i < entries.size(); i++)
		{
			try
			{
				uTarget.addEntry(entries.get(i), true);
			}
			catch (DuplicateIdException e)
			{
				
				e.printStackTrace();
			}
		}
		HashMap<String,String> sourceAttributes = uSource.getCustomAttributes();
		for (Map.Entry<String, String> entry : sourceAttributes.entrySet())
		{
			uTarget.addCustomAttribute(entry.getKey(), entry.getValue());
		}
		uTarget.save();
	}


	

	
}
