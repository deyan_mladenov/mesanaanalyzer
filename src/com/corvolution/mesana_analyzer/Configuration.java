package com.corvolution.mesana_analyzer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.corvolution.mesana_analyzer.analysis_functions.Analysis;
import com.corvolution.mesana_analyzer.data.HostServer;

/**
 * This class reads a java properties file. 
 * How to use this class: First get an Instance of this class with the method getInstance(). This class is a singleton, so there is only one isnatnce at runtime.
 * When instantiated for the first time the class reads the properties file and stores the values in local attributes. 
 * @author Deyan Mladenov
 *
 */
public class Configuration
{
	/**
	 * Configuration file name and key names, part of the key-value pairs in the configuration file.
	 */
	private final String CONFIG_FILE_NAME = "config.properties";
	
	private final String PATH_KEY = "pathToDataset";
	
	private final String FUNCTIONS_KEY = "namesOfAnalysisClasses";
			
	private final String PREFIX_KEY = "measurementIdPrefix";
	
	private final String DATA_ANALYZER_PATH = "dataAnalyzerPath";
	
	private final String DATA_ANALYZER_PARAMETERS = "dataAnalyzerParamateres";
	
	private final String REST_PATH = "restPath"; 
	
	private final String HOSTS_PORTS = "hostsPorts";
	
	private final String NUM_THREADS = "numThreads";
	
	private final String OVERWRITE = "overwrite";
	
	private final String LOG_LOCATION = "logLocation";
	
	private final String BAT_PATH = "serverBatPath";
	
	private final String FIXED_RATE = "fixedRate";
	
	
	/** Container fields for all properties read from the configuration file. */
	private String[] classNames = null;
	
	private String pathToData = null;
	
	private String measurementIdPrefix = null;
	
	private String dataAnalyzerPath = null;
	
	private String dataAnalyzerParameters = null;
	
	private String restPath = null;
	
	private List<HostServer> servers = null;
	
	private int threadsCount;
	
	private boolean overwrite;
	
	private String logFilesPath;
	
	private String serverBatchPath;
	
	private int fixedRate;
	
	/** This class is a singleton. Store the only instance in this private field */
	private static Configuration instance;
	/**
	 * Constructs new Configuration files and reads and stores the values from the file in local variables.
	 */
	private Configuration()
	{
		this.getValues();
 	}
	
	/** 
	 * Get the only instance of this singleton class 
	 * @return instance
	 */
	public synchronized static Configuration getInstance()
	{
		if (instance == null)
		{
			instance = new Configuration();
		}
		
		return instance;
	}

	/**
	 * This function uses a String array with the full names of all analysis classes and instantiates those classes 
	 * using the ClassLoader's loadClass() method.
	 * @return List with all classes implementing the Analysis interface.
	 */
	public synchronized ArrayList<Analysis> getAnalysisFunctions()
	{
		ArrayList<Analysis> analysisList = new ArrayList<>();

		ClassLoader classLoader = Analysis.class.getClassLoader();

		try
		{
			
			for (int i = 0; i < this.classNames.length; i++)
			{
				Analysis analysis = (Analysis) classLoader.loadClass(classNames[i].trim()).newInstance();
				analysisList.add(analysis);
			}
		}
		catch (InstantiationException | IllegalAccessException | ClassNotFoundException e)
		{
			e.printStackTrace();
		}

		return analysisList;
	}
	
	/**
	 * Get the path to the folder containing the data.
	 * @return path to data
	 */
	public String getPathToData()
	{

		return this.pathToData;
	}

	/**
	 * Get measurementID prefix.
	 * @return MeasurementID prefix.
	 */
	public String getMeasurementIdPrefix()
	{
		return this.measurementIdPrefix;
	}
	
	/**
	 * Get class names as String array.
	 * @return String array with full class names
	 */
	public String[] getClassNames()
	{
		return classNames;
	}
	
	public String getDataAnalyzerPath()
	{
		return dataAnalyzerPath;
	}

	public String getDataAnalyzerParameters()
	{
		return dataAnalyzerParameters;
	}
	
	public String getRestPath()
	{
		return restPath;
	}

	/**
	 * Gets a list of servers.
	 * If a server is a local one, the host field in the class HostServer is null
	 * @return servers
	 */
	public List<HostServer> getServers()
	{
		return servers;
	}

	public int getThreadsCount()
	{
		return threadsCount;
	}


	public boolean getOverwrite()
	{
		return overwrite;
	}

	public String getLogFilesPath()
	{
		return logFilesPath;
	}


	public String getServerBatchPath()
	{
		return serverBatchPath;
	}

	public int getFixedRate()
	{
		return fixedRate;
	}

	/**
	 * Read the properties file and store the values in local variables.
	 */
	public void getValues()
	{

		try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(this.CONFIG_FILE_NAME))
		{

			Properties prop = new Properties();

			if (inputStream != null)
			{
				prop.load(inputStream);
			}
			else
			{
				// TODO Log message
				throw new FileNotFoundException(
						"property file '" + this.CONFIG_FILE_NAME + "' not found in the classpath");
			}
			
			this.classNames = prop.getProperty(this.FUNCTIONS_KEY).split(",");
			
			this.pathToData = prop.getProperty(this.PATH_KEY).trim();
			
			this.measurementIdPrefix = prop.getProperty(this.PREFIX_KEY).trim();
			
			this.dataAnalyzerPath = prop.getProperty(DATA_ANALYZER_PATH);
			
			this.dataAnalyzerParameters = prop.getProperty(DATA_ANALYZER_PARAMETERS);
			
			this.restPath = prop.getProperty(REST_PATH);
			
			this.servers = this.parseHostsPorts(prop.getProperty(HOSTS_PORTS).split(","));
			
			this.threadsCount = Integer.parseInt(prop.getProperty(NUM_THREADS, "2"));
			
			int overwrite = Integer.parseInt(prop.getProperty(OVERWRITE));
			
			if (overwrite == 0) this.overwrite = false;
			else this.overwrite = true;
			
			this.logFilesPath = prop.getProperty(LOG_LOCATION);
			
			this.serverBatchPath = prop.getProperty(BAT_PATH);
			
			this.fixedRate = Integer.parseInt(prop.getProperty(FIXED_RATE));

		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Initializes HostServer object from a list of host name and port pairs.
	 * If a host name is defined as local, the host name field is set as null.
	 * @param hostsPorts
	 * @return List of HostServer objects.
	 */
	private List<HostServer> parseHostsPorts(String [] hostsPorts)
	{
		List<HostServer> servers = new ArrayList<HostServer>();
		
		for (int i = 0; i < hostsPorts.length; i++)
		{
			if (i % 2 == 0)
			{
				if (hostsPorts[i].trim().equals("local"))
				{
					HostServer server = new HostServer(null, Integer.parseInt(hostsPorts[i+1].trim())); // TODO exception
					servers.add(server);
				}
				else
				{
					HostServer server = new HostServer(hostsPorts[i], Integer.parseInt(hostsPorts[i+1].trim())); // TODO
					servers.add(server);
				}
			}
			else
			{
				continue;
			}
		}
		
		return servers;
	}

}
