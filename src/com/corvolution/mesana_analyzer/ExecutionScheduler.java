package com.corvolution.mesana_analyzer;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * This class provides a scheduling mechanism for the task of analyzing measurements. 
 * One option is to perform analysis at fixed rate of N hours.
 * @author Deyan Mladenov
 *
 */
public class ExecutionScheduler
{
	static Logger logger = Logger.getLogger(ExecutionScheduler.class.getName());
	
	/**
	 * Schedule execution at fixed rate. Read rate value from configuration file.
	 */
	public void scheduledExecution()
	{
		AnalyzerExecutor task = new AnalyzerExecutor();
		ScheduledExecutorService scheduledExecutor = Executors.newScheduledThreadPool(1);
		int rate = Configuration.getInstance().getFixedRate();
		scheduledExecutor.scheduleAtFixedRate(task, 0, rate, TimeUnit.HOURS);
		
		//Shutdown hook allows the programmer to define a behavior when the user terminates the program.
		Runtime.getRuntime().addShutdownHook(new Thread() 
			{
				public void run()
				{
					scheduledExecutor.shutdown();
					try
					{
						if (!scheduledExecutor.awaitTermination(1 ,TimeUnit.MINUTES)) 
						{
							logger.log(Level.SEVERE, "Scheduled Execution did not nerminate in the specified time");
							List<Runnable> droppedTasks = scheduledExecutor.shutdownNow();
							logger.log(Level.SEVERE, "Executor was abruptly shut down. " + droppedTasks.size() + " tasks will not be executed.");
						}
					}
					catch (InterruptedException e)
					{
						logger.log(Level.SEVERE, "Execution service shutdown was interrupted.", e);
						e.printStackTrace();
					}
				}
			});
	}

}
