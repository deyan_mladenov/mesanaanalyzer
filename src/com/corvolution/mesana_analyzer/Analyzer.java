package com.corvolution.mesana_analyzer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONObject;

import com.corvolution.mesana_analyzer.analysis_functions.Analysis;
import com.corvolution.mesana_analyzer.data.HostServer;
import com.corvolution.mesana_analyzer.data.Measurement;
import com.corvolution.mesana_analyzer.utility.LoggerConfigurator;
import com.corvolution.mesana_analyzer.utility.RestApiConnector;

/**
 * This class manages the analysis functions through the Analysis interface. It gets a measurement 
 * and executes a sequential analysis of this measurement. 
 * This class implements the runnable interface, therefore can be delegated to a threaded execution.
 * @author Deyan Mladenov
 *
 */
public class Analyzer implements Runnable
{
	
	private final static String ANALYZING = "ANALYZING";
	
	private final static String STATE_WAIT_REPORT = "WAIT_FOR_REPORT";
	
	private final static String STATE_FAILURE = "FAILURE";
	
	private Logger logger = Logger.getLogger(Analyzer.class.getName());

	private String measurement;
	
	private boolean overwrite;
	
	private static String login = null;
	
	private ArrayBlockingQueue<HostServer> servers;
	
	
	private String fullPathToMeasurement;
	
	String tag;


	/**
	 * Initialize this class.
	 */
	public Analyzer(String measurement, ArrayBlockingQueue<HostServer> servers)
	{
		this.measurement = measurement;
		this.servers = servers;
		this.fullPathToMeasurement = Configuration.getInstance().getPathToData() + Configuration.getInstance().getMeasurementIdPrefix() + this.measurement; 
		this.tag = "Measurement ID: "+ Configuration.getInstance().getMeasurementIdPrefix() + this.measurement + " --- ";
		this.overwrite = Configuration.getInstance().getOverwrite();
	}
	
	public Analyzer()
	{
	}
	
	@Override
	public void run()
	{
		this.singleAnalysis(this.measurement, Configuration.getInstance().getAnalysisFunctions());
		
	}
	
	/**
	public void analyseMeasurements()
	{
		if (this.measurements == null)
		{
			return;
		}
		
		for (int i = this.measurements.size() - 1; i >= 0; --i)
		{
			String measurementId = this.measurements.get(i).getId();
			ArrayList<Analysis> allAnalyzes = Configuration.getInstance().getAnalysisFunctions();
			this.singleAnalysis(measurementId, allAnalyzes);
		}
	}*/

	/**
	 * Start a new analysis on the given measurement. This method decides during runtime on the execution order of the analysis classes.
	 * @param measurementId
	 * @param todo
	 */
	public void singleAnalysis(String measurementId, ArrayList<Analysis> todo)
	{
		HostServer server = null;
		try
		{
			server = this.servers.take();
		}
		catch (InterruptedException e)
		{
			Thread.currentThread().interrupt();
		}
		this.restApiUpdate(ANALYZING, measurementId);
		String message =  server == null ? "no server":server.toString();
		logger.log(Level.INFO, tag + "Starting analysis on Server: " + message);
		ArrayList<Analysis> done = new ArrayList<Analysis>();

		if (todo == null) 
		{
			logger.log(Level.SEVERE, "{[0]} list with all initialized analysis functions is empty", new Object[]{this.tag} );
			return;
		}
		Iterator<Analysis> iter = todo.iterator();

		/*
		 * With each iteration functions, whose preconditions are in the done list, are executed, removed from the todo list and
		 * added to done list.
		 */
		while (!todo.isEmpty())
		{
			iter = todo.iterator();
			while (iter.hasNext())
			{
				Analysis current = iter.next();
				if (contains(done, current.getPrecondition()))
				{
					//current.setMeasurementId();
					boolean finished = current.start(this.fullPathToMeasurement, server, this.overwrite);
					if (!finished)
					{
						logger.log(Level.SEVERE, "{0} Error in analysis:{1}. Look into the errorLog file for further detail.", new Object[]{tag, current.getClass().getSimpleName()});
						this.restApiUpdate(STATE_FAILURE, measurementId);
						return;
					}

					done.add(current);
					iter.remove();
				}
			}
		}
		try
		{
			this.servers.put(server);
		}
		catch (InterruptedException e)
		{
		}
		this.restApiUpdate(STATE_WAIT_REPORT, measurementId);
		logger.log(Level.INFO,  tag + "Finished analysis on Server: " + message);
	}

	/**
	 * Check if a list of elements is contained within a container list.
	 * @param container
	 * @param elements
	 * @return true if all elements are in the container list
	 */
	private boolean contains(ArrayList<Analysis> container, ArrayList<Analysis> elements)
	{
		if (elements != null)
		{
			Iterator<Analysis> iter = elements.iterator();
			while (iter.hasNext())
			{
				// If the container with finished examinations does not contain one of the examinations in the precondition list return false;
				if (!container.contains(iter.next()))
				{
					return false;
				}

			}
			return true;
		}
		else 
		{
			return true;
		}
	}
	
	@SuppressWarnings("unchecked")
	public void restApiUpdate(String state, String mId)
	{
		RestApiConnector restApi = new RestApiConnector();
		JSONObject json = new JSONObject();
		json.put("state", state);
		json.put("user", login);
		String jsonString = json.toJSONString();
		String sURL = Configuration.getInstance().getRestPath() + "measurements/" + mId + "/";
		try
		{
			restApi.putMethod(jsonString, sURL);
		}
		catch (Exception e)
		{
			Logger errorLog = LoggerConfigurator.getErrorLogger();
			errorLog.log(Level.SEVERE, "{0} Exception thrown while updating via REST Api: {1}", new Object[]{tag, e.toString()});
			//e.printStackTrace();
		}
	}

	public void setOverwrite(boolean overwrite)
	{
		this.overwrite = overwrite;
	}
	
	@Override
	public boolean equals(Object arg0)
	{
		if (arg0 == null || !(arg0 instanceof Analyzer))
		{
			return false;
		}
		else
		{
			Analyzer other = (Analyzer) arg0;
			if (other.getMeasurement().equals(this.getMeasurement()))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	public String getMeasurement()
	{
		return measurement;
	}


}
