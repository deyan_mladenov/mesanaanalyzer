


import com.mathworks.toolbox.javabuilder.*;
import com.mathworks.toolbox.javabuilder.remoting.*;
import java.rmi.*;
import java.rmi.registry.*;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.TimeUnit;

import afdetection.*;
import dtr.*;
import mesana.dataanalyzer.*;
import mesana.serverexit.*;
import mmsqa.*;
import convert_ekgMove3_to_DekomTex.*;

public class MesanaAnalyzerLocalServer implements ServerShutdown
{
	private static final String DTR = "DTR";
	
	private static final String AF = "AF";
	
	private static final String QA = "QA";
	
	private static final String LS = "LS";
	
	private static final String DC = "DC";
	
	private static final String SERVER = "SERVER";
	
	static MedicalAnalysisImplementation dtr;
	
	static MatlabAfDetection afanalysis;
	
	static QuestionnaireAnalysisImplementation qaImpl;
	
	static LifestyleAnalysisRemoteImpl lifestyleRemote;
	
	static Class1 converter;
	
	static MesanaAnalyzerLocalServer mals;
	
	static int port;
	
	static Registry reg = null;
	
	public MesanaAnalyzerLocalServer()
	{
		super();
	}

	public static void main(String[] args)
	{
		
		System.out.println("\nPlease wait for the server registration notification.");
        long starttime = System.nanoTime();
        
        port = Integer.parseInt(args[0]);
        
        try
        {
        	dtr = new MedicalAnalysisImplementation();
        	MedicalAnalysisImplementationRemote dtrRemote = (MedicalAnalysisImplementationRemote) RemoteProxy.newProxyFor(dtr, MedicalAnalysisImplementationRemote.class, false);
        	
        	afanalysis = new MatlabAfDetection();
        	MatlabAfDetectionRemote afanalysisRemote = (MatlabAfDetectionRemote) RemoteProxy.newProxyFor(afanalysis, MatlabAfDetectionRemote.class, false);
        	
        	qaImpl = new QuestionnaireAnalysisImplementation();
        	QuestionnaireAnalysisImplementationRemote qaImplremote = (QuestionnaireAnalysisImplementationRemote) RemoteProxy.newProxyFor(qaImpl,
        			QuestionnaireAnalysisImplementationRemote.class, false);
        	
        	lifestyleRemote = new LifestyleAnalysisRemoteImpl();
        	LifestyleAnalysisRemote lifestyleStub = (LifestyleAnalysisRemote) UnicastRemoteObject.exportObject(lifestyleRemote, Integer.parseInt(args[0]));
        	
        	converter = new Class1();
        	Class1Remote converterRemote = RemoteProxy.newProxyFor(converter, Class1Remote.class, false);
        	
        	mals = new MesanaAnalyzerLocalServer();
        	ServerShutdown quit = (ServerShutdown) UnicastRemoteObject.exportObject(mals, Integer.parseInt(args[0]));
        	
        	reg = LocateRegistry.createRegistry(Integer.parseInt(args[0]));
            reg.rebind(DTR, dtrRemote);
            reg.rebind(AF, afanalysisRemote);
            reg.rebind(QA, qaImplremote);
            reg.rebind(LS, lifestyleStub);
            reg.rebind(DC, converterRemote);
            reg.rebind(SERVER, quit);
            
            long difference = System.nanoTime() - starttime;
            System.out.println("Server registered and running successfully. Time needed " + String.format("%d sec", TimeUnit.NANOSECONDS.toSeconds(difference)-
            		TimeUnit.MINUTES.toSeconds(TimeUnit.NANOSECONDS.toMinutes(difference))));
        	
        }
        catch(RemoteException remote_ex)
        {
            System.out.println("\nRemoteException being thrown...\n");
            remote_ex.printStackTrace();            
        }        
        catch(MWException mw_ex)
        {
            System.out.println("\nMWException being thrown...\n");
            mw_ex.printStackTrace();
        }

	}

	@Override
	public void exitServer() throws RemoteException
	{
		
		try
		{
			reg.unbind(AF);
			reg.unbind(DC);
			reg.unbind(DTR);
			reg.unbind(LS);
			reg.unbind(DC);
			reg.unbind(SERVER);
		
			afanalysis.dispose();
			dtr.dispose();
			qaImpl.dispose();
			converter.dispose();
			UnicastRemoteObject.unexportObject(lifestyleRemote, false);
			UnicastRemoteObject.unexportObject(mals, false);
		}
		catch (NotBoundException e)
		{
		    throw new RemoteException("Could not unregister service, quiting anyway", e);
		}
		catch (NoSuchObjectException e1)
		{
			throw new RemoteException("No such object in registry with given mapping found.", e1);
		}
		finally
		{
			System.out.println("SHUTTING DOWN. GET TO THE CHOPEEERR");
		
			System.exit(0);
		}
		System.out.println("SHUTTING DOWN. GET TO THE CHOPEEERR");
		
		System.exit(0);
		
		
		
	}

}
