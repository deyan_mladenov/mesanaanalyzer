
import com.mathworks.toolbox.javabuilder.*;
import com.mathworks.toolbox.javabuilder.remoting.*;
import java.rmi.*;
import java.rmi.registry.*;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.TimeUnit;

import afdetection.*;
import convert_ekgMove3_to_DekomTex.Class1;
import convert_ekgMove3_to_DekomTex.Class1Remote;
import dtr.*;
import mesana.dataanalyzer.*;
import mmsqa.*;

public class MesanaAnalyzerRemoteServer
{
	private static final String DTR = "DTR";
	
	private static final String AF = "AF";
	
	private static final String QA = "QA";
	
	private static final String LS = "LS";
	
	private static final String DC = "DC";

	
	static MedicalAnalysisImplementation dtr;
	
	static MatlabAfDetection afanalysis;
	
	static QuestionnaireAnalysisImplementation qaImpl;
	
	static LifestyleAnalysisRemoteImpl lifestyleRemote;
	
	static Class1 converter;


	public static void main(String[] args)
	{
		
		System.out.println("\nPlease wait for the server registration notification.");
        long starttime = System.nanoTime();
        Registry reg = null;
        
        try
        {
        	dtr = new MedicalAnalysisImplementation();
        	MedicalAnalysisImplementationRemote dtrRemote = (MedicalAnalysisImplementationRemote) RemoteProxy.newProxyFor(dtr, MedicalAnalysisImplementationRemote.class, false);
        	
        	afanalysis = new MatlabAfDetection();
        	MatlabAfDetectionRemote afanalysisRemote = (MatlabAfDetectionRemote) RemoteProxy.newProxyFor(afanalysis, MatlabAfDetectionRemote.class, false);
        	
        	qaImpl = new QuestionnaireAnalysisImplementation();
        	QuestionnaireAnalysisImplementationRemote qaImplremote = (QuestionnaireAnalysisImplementationRemote) RemoteProxy.newProxyFor(qaImpl,
        			QuestionnaireAnalysisImplementationRemote.class, false);
        	
        	lifestyleRemote = new LifestyleAnalysisRemoteImpl();
        	LifestyleAnalysisRemote lifestyleStub = (LifestyleAnalysisRemote) UnicastRemoteObject.exportObject(lifestyleRemote, Integer.parseInt(args[0]));
        	
        	converter = new Class1();
        	Class1Remote converterRemote = RemoteProxy.newProxyFor(converter, Class1Remote.class, false);
        	
        	reg = LocateRegistry.getRegistry(Integer.parseInt(args[0]));
            reg.rebind(DTR, dtrRemote);
            reg.rebind(AF, afanalysisRemote);
            reg.rebind(QA, qaImplremote);
            reg.rebind(LS, lifestyleStub);
            reg.rebind(DC, converterRemote);

            
            long difference = System.nanoTime() - starttime;
            System.out.println("Server registered and running successfully. Time needed " + String.format("%d sec", TimeUnit.NANOSECONDS.toSeconds(difference)-
            		TimeUnit.MINUTES.toSeconds(TimeUnit.NANOSECONDS.toMinutes(difference))));
        	
        }
        catch(RemoteException remote_ex)
        {
            System.out.println("\nRemoteException being thrown...\n");
            remote_ex.printStackTrace();            
        }        
        catch(MWException mw_ex)
        {
            System.out.println("\nMWException being thrown...\n");
            mw_ex.printStackTrace();
        }

	}

}
