MesanaAnalzyer

The MesanaAnalzyer is a framework build to analyze the measurements retrieved from the mesana sensors. For this purpose it uses several analysis functions, implemented in Matlab and deployed to .jar files with the Matlab Builder JA.
The frameworks main task is to execute all analyses in the right order and check for errors and execution completeness in the resulting measurement files. Appart from that, the MesanaAnalyzer is capable of running the analysis of several measurements in parallel, thus increasing the overall performance.